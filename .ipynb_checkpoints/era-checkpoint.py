import datetime
import xarray as xr 
import numpy as np
from scipy.interpolate import griddata

main_PATH = '/home1/datawork/vlheureu/'
PATH_era  = "/home/datawork-cersat-public/cache/project/mpc-sentinel1/data/ancillary/era5/members/with_air_pressure/"
PATH_era_members = PATH_era + "/members/"

class ERA(object):
    def __init__(self,obs):
        self.obs = obs
        self.time_obs = obs.time_obs 
        self.name = "ERA5"

    def getTime(self):
        temps_sar = self.time_obs.split(' - ')[1]
        unixSAR = (datetime.datetime(int(self.time_obs[0:4]),int(self.time_obs[5:7]),int(self.time_obs[8:10]),
                                int(temps_sar[0:2]),int(temps_sar[3:5]),int(temps_sar[6:8])) - datetime.datetime(1970,1,1)).total_seconds()
        
        if unixSAR % (3*60*60) <=  1.5*60*60 :
            ts = int(unixSAR-unixSAR % (3*60*60))
            self.time_model = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d - %H:%M:%S UTC')
        else : 
            ts = int(unixSAR + (3*60*60-unixSAR % (3*60*60)))
            self.time_model =  datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d - %H:%M:%S UTC')
        return self.time_model         
        
    def getMembers(self):
        time = self.getTime()
        date_era = time.split(' - ')[0]
        yday = datetime.date(int(date_era[0:4]),int(date_era[5:7]),int(date_era[8:10])).timetuple().tm_yday
        try :
                print(PATH_era_members+date_era[0:4]+"/"+str(yday).zfill(3)+"/ECMWF_ERA5_surface_wind_"+date_era.replace('-','')+"_members.nc")

                self.members_nc = xr.open_dataset(PATH_era_members+date_era[0:4]+"/"+str(yday).zfill(3)+"/ECMWF_ERA5_surface_wind_"+date_era.replace('-','')+"_members.nc")
        except : 
                self.members_nc = xr.open_dataset(PATH_era_members.replace("/members/with_air_pressure/","")+date_era[0:4]+"/"+str(yday).zfill(3)+"/ECMWF_ERA5_surface_wind_"+date_era.replace('-','')+"_members.nc")

        return self.members_nc   
    
    def load(self,res):
        ds_era = self.getMembers()
        temps_era = int(self.time_model.split(' - ')[1][0:2])
        if (temps_era%3 != 0):
            print("Erreur temps_era ", temps_era)
            return False
        time_ind = temps_era//3
        
        ## LON,LAT MEMBERS at 0.5°
        lon = np.array(ds_era['longitude'])-180
        lat = np.flip(np.array(ds_era['latitude']))
        index_lat_min  = np.argmin(np.abs(lat-self.obs.grid_lat_min))-10
        index_lat_max  = np.argmin(np.abs(lat-self.obs.grid_lat_max))+10
        index_lon_min  = np.argmin(np.abs(lon-self.obs.grid_lon_min))-10
        index_lon_max  = np.argmin(np.abs(lon-self.obs.grid_lon_max))+10

        self.lonmem = lon[index_lon_min:index_lon_max]
        self.latmem = lat[index_lat_min:index_lat_max]   
        self.LONmem,self.LATmem = np.meshgrid(self.lonmem,self.latmem)

        xb_u_era = np.roll(np.mean(np.array(ds_era['u10'])[time_ind],axis=0),360,axis=1)
        xb_v_era = np.roll(np.mean(np.array(ds_era['v10'])[time_ind],axis=0),360,axis=1)
        xb_u_era = np.flip(xb_u_era,axis=0)[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
        xb_v_era = np.flip(xb_v_era,axis=0)[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
        spd_mean_era = np.sqrt(xb_u_era**2+xb_v_era**2)      
        pts = np.array([self.LONmem.flatten(),self.LATmem.flatten()]).T # pts décrivent current grid  
        
        xb_u = griddata(pts,xb_u_era.flatten(),(self.obs.LON_grid,self.obs.LAT_grid),method="linear")
        xb_v = griddata(pts,xb_v_era.flatten(),(self.obs.LON_grid,self.obs.LAT_grid),method="linear")
        spd_mean = griddata(pts,spd_mean_era.flatten(),(self.obs.LON_grid,self.obs.LAT_grid),method="linear")
        self.spd_mean = spd_mean     
        
        
        members_u =  np.empty(shape=(len(self.lonmem)*len(self.latmem),10))
        members_v =  np.empty(shape=(len(self.lonmem)*len(self.latmem),10))
        members_spd = np.empty(shape=(len(self.lonmem)*len(self.latmem),10))
        members_pr =  np.empty(shape=(len(self.lonmem)*len(self.latmem),10))

        for i in range(10):
            current_u = np.roll(np.array(ds_era['u10'])[time_ind][i],360,axis=1)
            current_v = np.roll(np.array(ds_era['v10'])[time_ind][i],360,axis=1)            
            current_u = np.flip(current_u,axis=0)[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
            current_v = np.flip(current_v,axis=0)[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
            current_spd = np.sqrt(current_u**2+current_v**2)
            #current_pressure = np.roll(np.array(ds_era['sp'])[time_ind][i],360,axis=1)
            #current_pressure = np.flip(current_pressure,axis=0)[index_lat_min:index_lat_max,index_lon_min:index_lon_max]

            members_u[:,i] = current_u.flatten()
            members_v[:,i] = current_v.flatten()  
            members_spd[:,i] = current_spd.flatten()
            #members_pr[:,i] = current_pressure.flatten()

        lon_grid = np.arange(self.obs.grid_lon_min,self.obs.grid_lon_max,res)
        lat_grid = np.arange(self.obs.grid_lat_min,self.obs.grid_lat_max,res)

        self.xb_u = xb_u/spd_mean
        self.xb_v = xb_v/spd_mean
        
        new_array_u = np.empty(shape=(len(lon_grid)*len(lat_grid),10))
        new_array_v = np.empty(shape=(len(lon_grid)*len(lat_grid),10))
        new_array_spd = np.empty(shape=(len(lon_grid)*len(lat_grid),10))
        #new_array_pr = np.empty(shape=(len(lon_grid)*len(lat_grid),10))

        for i in range (0,10):
            new_array_u[:,i] = griddata(pts,members_u[:,i],(self.obs.LON_grid,self.obs.LAT_grid),method="linear").flatten()
            new_array_v[:,i] = griddata(pts,members_v[:,i],(self.obs.LON_grid,self.obs.LAT_grid),method="linear").flatten()
            new_array_spd[:,i] = griddata(pts,members_spd[:,i],(self.obs.LON_grid,self.obs.LAT_grid),method="linear").flatten()
            new_array_u[:,i] = new_array_u[:,i]/new_array_spd[:,i]
            new_array_v[:,i] = new_array_v[:,i]/new_array_spd[:,i]
            #new_array_pr[:,i] = griddata(pts,members_pr[:,i],(self.obs.LON_grid,self.obs.LAT_grid),method="linear").flatten()

        #self.members_pr = new_array_pr
        self.members_u = new_array_u
        self.members_v = new_array_v