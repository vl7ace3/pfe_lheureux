#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 14:12:51 2021

@author: vincelhx
"""

import numpy as np
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.pyplot as plt
SMALL_SIZE = 12
MEDIUM_SIZE = 15
BIGGER_SIZE = 17
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

class MembersAnalysis(object):
    def __init__(self,shift,Pu,Pv):
        self.shift=shift
        self.Pu = Pu
        self.Pv = Pv
        
    def DisplaySpread(self):       
        spread_u = np.diag(self.Pu).reshape(self.shift.LON_dec.shape)
        spread_v = np.diag(self.Pv).reshape(self.shift.LON_dec.shape)
        plt.figure(figsize=(30,15));
        plt.subplot(121)
        im = plt.pcolormesh(self.shift.LON_dec,self.shift.LAT_dec,spread_u,cmap =plt.cm.binary,vmin=-0.2,vmax=0.2) ;  plt.colorbar(im)
        plt.streamplot(self.shift.LON_dec,self.shift.LAT_dec,self.shift._XB_U_after,self.shift._XB_V_after)
        plt.subplot(122)
        im = plt.pcolormesh(self.shift.LON_dec,self.shift.LAT_dec,spread_v,cmap =plt.cm.binary,vmin=-0.2,vmax=0.2) ;  plt.colorbar(im)
        plt.streamplot(self.shift.LON_dec,self.shift.LAT_dec,self.shift._XB_U_after,self.shift._XB_V_after)
        
    def DisplayOnePt(self):
        p=10

        ridx_pt1 = np.random.randint(0,self.shift.LON_dec.shape[0]) 
        ridx_pt2 = np.random.randint(0,self.shift.LON_dec.shape[1]) 
        ridx_pt = ridx_pt1*ridx_pt2
        ln = self.shift.LON_dec[0,ridx_pt2]
        lt = self.shift.LAT_dec[ridx_pt1,0]
        fig = plt.figure(figsize=(30,15))   
        crs = ccrs.PlateCarree()
        
        ax = fig.add_subplot(1, 2, 1, projection=crs)
        ax.plot(ln,lt,'go',markersize=10) ; ax.set_title('Zonal covariance ERA5')
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        im=ax.pcolormesh(self.shift.LON_dec,self.shift.LAT_dec,self.Pu[:,ridx_pt].reshape(self.shift.LON_dec.shape),vmax=0.01,vmin=-0.01,cmap=plt.cm.seismic)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        # gl.xlines = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.quiver(self.shift.LON_dec[::p,::p],self.shift.LAT_dec[::p,::p],self.shift._XB_U_after[::p,::p],self.shift._XB_V_after[::p,::p],scale=50)
        plt.colorbar(im)
    
        ax = fig.add_subplot(1, 2, 2, projection=crs)
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        ax.plot(ln,lt,'go',markersize=10) ; ax.set_title('Meridional covariance ERA5')
        im=ax.pcolormesh(self.shift.LON_dec,self.shift.LAT_dec,self.Pv[:,ridx_pt].reshape(self.shift.LON_dec.shape),vmax=0.01,vmin=-0.01,cmap=plt.cm.seismic)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.xlabels_top = False
        gl.ylabels_right = False
        # gl.xlines = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        plt.quiver(self.shift.LON_dec[::p,::p],self.shift.LAT_dec[::p,::p],self.shift._XB_U_after[::p,::p],self.shift._XB_V_after[::p,::p],scale=50)
        plt.colorbar(im)
    
