#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: vlheureu
"""
import numpy as np 
import xarray as xr 
import matplotlib.pyplot as plt
from scipy.interpolate import griddata
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.patches as mpatches

class SAR(object):
    def __init__(self,fn):
        self.fn =  fn
        self.ds_obs = xr.open_dataset(self.fn)
        self.lon_obs = np.array(self.ds_obs['lon'])
        self.lat_obs = np.array(self.ds_obs['lat'])
        self.winddir_obs = np.array(self.ds_obs['wind_streaks_orientation'][0])
        self.stddev_obs = np.array(self.ds_obs['wind_streaks_orientation_stddev'][0])
        self.windspeed_obs = np.array(self.ds_obs['wind_speed'][0])
        self.time_obs = np.datetime_as_string(self.ds_obs['time'][0], unit='s').replace('T',' - ') +' UTC'
        #self.mask = np.isfinite(winddir_obs)
        
    def load(self,res):
        """
        Generate observation data at choosen resolution

        Parameters
        ----------
        res : float
            grid resolution

        Returns
        -------
        bool
            True if completed.

        """
        self.marge = 2*res*np.around(1/res) # ~2°

        self.u_obs = np.cos(-np.pi/2-np.deg2rad(self.winddir_obs))
        self.v_obs = np.sin(-np.pi/2-np.deg2rad(self.winddir_obs))
        
        _lons,_lats = np.meshgrid(self.lon_obs,self.lat_obs)
        pts = (_lons.flatten(),_lats.flatten()) 
        
        min_lon = np.min(self.lon_obs)-np.min(self.lon_obs)%res
        max_lon = np.max(self.lon_obs)-np.max(self.lon_obs)%res
        min_lat = np.min(self.lat_obs)-np.min(self.lat_obs)%res
        max_lat = np.max(self.lat_obs)-np.max(self.lat_obs)%res
        
        self.grid_lat_min = np.around(min_lat-self.marge,2)
        self.grid_lat_max = np.around(max_lat+self.marge+res,2)
        self.grid_lon_min = np.around(min_lon-self.marge,2)
        self.grid_lon_max = np.around(max_lon+self.marge+res,2)

        self.LON_grid,self.LAT_grid = np.meshgrid(np.arange(self.grid_lon_min,self.grid_lon_max,res), \
                                                  np.arange(self.grid_lat_min,self.grid_lat_max,res))    
            
        self.y0_u_sar = griddata(pts,self.u_obs.flatten(),(self.LON_grid,self.LAT_grid),"linear")
        self.y0_v_sar = griddata(pts,self.v_obs.flatten(),(self.LON_grid,self.LAT_grid),"linear")
        self.y0_spd = griddata(pts,self.windspeed_obs.flatten(),(self.LON_grid,self.LAT_grid),"linear")
        self.y0_stddev_obs = griddata(pts,self.stddev_obs.flatten(),(self.LON_grid,self.LAT_grid),"linear")

        return True

    def display(self):
        """
        Display sar streaks and wind direction uncertainty

        Returns
        -------
        None.

        """
        param=20
        #lons,lats = np.meshgrid(self.lon_obs,self.lat_obs)
        fig = plt.figure("SAR_winddir",figsize=(20,10))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon_obs,self.lat_obs,self.stddev_obs,cmap=plt.cm.RdYlGn_r); 
        cbar = plt.colorbar(im) ; cbar.set_label("Wind direction Uncertainties [°]")
        ax.quiver(self.lon_obs[::param],self.lat_obs[::param], \
                  self.u_obs[::param,::param],self.v_obs[::param,::param],color="blue",headwidth =0,scale = 30)
        patch_sar = mpatches.Patch(color = 'blue',label = 'original SAR wind streaks')
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5,scale ='10m')
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5,scale ='10m', linewidth=1) 
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER

        ax.set_title(" - Orignal SAR wind streaks direction - " + self.time_obs)
        ax.legend(handles=[patch_sar],loc='upper right')
        
        