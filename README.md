## Basis

In this project, we used Optimal Interpolation (OI) to combine two sources of data : 

1- SAR L2-product wind orientation (+ uncertainty estimation) derived from the streaks 
(method of Koch - 2004) : https://www.hereon.de/imperia/md/content/gkss/institut_fuer_kuestenforschung/ksd/paper/kochw_ieee_2004.pdf)

2- ERA5 zonal and meridional wind normalized components from ERA5 ensemble model (10 members).

To start the code, inputs are : 

- "inputfile" : input L2-IFREMER filename, ll_gd format 
- "res" : OI grid spacing (e.g. 0.16 // 0.25), in degrees 
- "model_name" : name of the model to use (era5 // mars) 
- "uv" : use of zonal and meridional component in the same state vector (True // False) 
- "lcz" : use of localization method (True // False) 
- "c" : value of the c parameter for localization (e.g. 100) 
- "desroziers" : use of Desroziers itterative method (True // False) 
- "localization_method" : choice of the localization method (0 since there is only one used) 
- "R_function_method": choice of the function to describe R matrix (use 0) 
- "translation_method" : choice of the function to translate member together and on the SAR (use 0 for no translation, 3 for translation using Gamma1) 

Example of command line to use the code : 

```bash
python main.py --res 0.16 --model_name era5 --uv True --desroziers True --lcz True --c 100 --translation_method 0 --inputfile /home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc
```

## Parallel computing

To compute this algorithm for multiple SAR use-cases (in parallel), I used `prun` : 


```bash
prun -e /home1/datahome/vlheureu/gitlab/pfe_lheureux/pbs/0.16/exe_translation_center_100.pbs --split-max-jobs 1 /home1/datahome/vlheureu/OI_2021/listings/listing_oi_centers.txt 
```
with ```exe_translation_center_100.pbs``` being :

```bash
#!/bin/bash
#PBS -l walltime=2:00:00
#PBS -l mem=40g
#PBS -N job_f
#PBS -j oe
#PBS -m n
#PBS -q ftp

echo "dollar1 "$1

pybin=/home1/datawork/vlheureu/conda/envs/env_vl_36/bin/python3
$pybin /home1/datahome/vlheureu/gitlab/pfe_lheureux/main.py --res 0.16 --model_name era5 --uv True --desroziers True --lcz True --c 100 --translation_method 3 --inputfile $1
```

## Further development

Runnig OI using L1-GRD and xsar/xsarsea libraries to optimize the treatment. 



