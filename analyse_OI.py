#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 15:48:15 2021

@author: vincelhx
"""

import sys 
sys.path.append("/home1/datahome/vlheureu/gitlab/pfe_lheureux/")

import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt 
import matplotlib.patches as mpatches
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import os 
from scipy.interpolate import griddata
import xarray as xr
from matplotlib.patches import Rectangle
from tools import getOutput
from shapely import geometry


SMALL_SIZE = 12
MEDIUM_SIZE = 15
BIGGER_SIZE = 17
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=MEDIUM_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

import warnings
warnings.filterwarnings("ignore")


global_folder = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/oi/final2_"
path_saving = global_folder +"/results/"

def isCrossingMeridian(lon):
    """
    check if a longitude of the array is crossing meridian
    Parameters
    ----------
    lon : array(float)
        array of longitudes.

    Returns
    -------
    bool
        True if its crossing meridian, else False.
    """
    lon = lon%360
    lon_min = np.min(lon)
    lon_max = np.max(lon)
    
    if ((lon_min < 15) & (lon_max > 345)) : 
        return True
    return False


class ANALYSE(object):
    def __init__(self,setup):
        self.setup = setup
        self.PATH_nc = setup["output_fn"]
        self.model_name = setup["model_name"]

        self.hurricane_name = setup["hurricane_name"] 
            
        self.res = str(setup["res"])
        self.PATH_ = self.PATH_nc.replace(".nc","/").replace("/results/","/analysis/")
        
        self.fn = self.PATH_nc.split('/')[-1]
        self.ds = xr.open_dataset(self.PATH_nc)
        self.longitude = np.array(self.ds.coords['LON_grid'])
        
        self.latitude = np.array(self.ds.coords['LAT_grid'])
        self.lon = np.array(self.ds.coords["lon"])
        self.lat = np.array(self.ds.coords["lat"])

        #model
        self.u_model = np.array(self.ds.variables['zonal_model_before_shift'])
        self.v_model = np.array(self.ds.variables['merid_model_before_shift'])
        self.u_model_shifted = np.array(self.ds.variables['zonal_model_after_shift'])
        self.v_model_shifted = np.array(self.ds.variables['merid_model_after_shift'])
        
        self.Pb_init = self.ds["Pb_init"].values
        diag_init = np.diag(self.Pb_init)
        self.Pb_u_var_init = diag_init[0:len(diag_init)//2].reshape(self.longitude.shape) #after shift 
        self.Pb_v_var_init = diag_init[len(diag_init)//2:].reshape(self.longitude.shape) #after shift 
        
        
        self.final_B = self.Pb_init

        
        #sar
        self.winddir_obs = np.array(self.ds['wind_streaks_orientation'])
        self.stddev_obs = np.array(self.ds['wind_streaks_orientation_stddev'])
        self.windspeed_obs = np.array(self.ds['wind_speed'])
        self.u_sar = np.cos(-np.pi/2-np.deg2rad(self.winddir_obs))
        self.v_sar = np.sin(-np.pi/2-np.deg2rad(self.winddir_obs))
        
        self.y0_u = np.array(self.ds['yo_u'])
        self.y0_v = np.array(self.ds['yo_v'])

        #oi 
        self.xa_u = np.array(self.ds["xa_u"])
        self.xa_v = np.array(self.ds["xa_v"])
        self.Pa_u = np.array(self.ds["Pa_u"])
        self.Pa_v = np.array(self.ds["Pa_v"])

        self.time_sar = self.ds.attrs["time_sar"]
        self.time_model = self.ds.attrs["time_model"]

        self.df = pd.read_csv(self.ds.attrs["path_associated_dataframe"])
        
        mask = np.where(np.isfinite(self.winddir_obs))
        #if isCrossingMeridian(self.lon) == False:
        #    self.lon = self.lon%360
        self.sar_polygon = geometry.MultiPoint([(self.lon[mask[1][cpt]], self.lat[mask[0][cpt]]) for cpt in range(mask[0].shape[0])]).convex_hull

    def createFolder(self):
        os.makedirs(self.PATH_, exist_ok = True)
    
    def displays_covs(self):
        m = 1
        if self.setup["localization"] : 
            Pb_localized = self.ds["Pb_localized"].values
            m+=1
            self.final_B = Pb_localized
                         
        if self.setup["desroziers"] : 
            Pb_Desroziers = self.ds["Pb_Desroziers"].values
            m+=1
            n = m
            self.final_B = Pb_Desroziers
            
            
        try :
            Pb_before_all = self.ds["Pb_before_all"].values
            diag_before_all = np.diag(Pb_before_all)
            self.Pb_u_var_before_all = diag_before_all[0:len(diag_before_all)//2].reshape(self.longitude.shape)
            self.Pb_v_var_before_all = diag_before_all[len(diag_before_all)//2:].reshape(self.longitude.shape)    
            
        except Exception as e : 
            Pb_before_all = self.ds["Pb_init"].values
            diag_before_all = np.diag(Pb_before_all)
            self.Pb_u_var_before_all = diag_before_all[0:len(diag_before_all)//2].reshape(self.longitude.shape)
            self.Pb_v_var_before_all = diag_before_all[len(diag_before_all)//2:].reshape(self.longitude.shape)    
        #m+=1
        #n = m
        
            

        diag_final = np.diag(self.final_B)
        self.Pb_u_var_final = diag_final[0:len(diag_final)//2].reshape(self.longitude.shape)
        self.Pb_v_var_final = diag_final[len(diag_final)//2:].reshape(self.longitude.shape)
        
        #self.Pb_u_zonal_final_spread = np.array(self.ds.variables['zonal_final_spread'])
        #self.Pb_v_merid_final_spread = np.array(self.ds.variables['merid_final_spread'])


        fig = plt.figure(figsize=(30,10))

        plt.subplot(1,m,1)
        im = plt.imshow(self.Pb_init,cmap=plt.cm.binary,vmin = 0, vmax = 0.005) ; plt.title("original cov. matrix")
        
        if self.setup["localization"] : 
            plt.subplot(1,m,2) 
            plt.imshow(Pb_localized,cmap=plt.cm.binary,vmin = 0, vmax = 0.005) ; plt.title("cov. matrix after localisation")
        
        if self.setup["desroziers"] : 
            plt.subplot(1,m,n)
            plt.imshow(Pb_Desroziers,cmap=plt.cm.binary,vmin = 0, vmax = 0.005) ; plt.title("cov. matrix after localisation & Desroziers iterations")


        #fig.colorbar(im, orientation="horizontal", pad=0.2)
        cbaxes = fig.add_axes([0.25, 0.075 , 0.5, 0.03]) 
        cb = plt.colorbar(im , cax = cbaxes,orientation='horizontal') ; cb.set_label("covariance value")
        plt.show()
        plt.savefig(self.PATH_+self.hurricane_name+"_"+self.time_sar+"_"+self.model_name+"covs_shape_.jpeg")
                
    
    def model_shift_effect(self):
        param = 20
        SAR_lon,SAR_lat = np.meshgrid(self.lon,self.lat)
        u_era = griddata((self.longitude.flatten(),self.latitude.flatten()), self.u_model.flatten(), (SAR_lon,SAR_lat))
        v_era = griddata((self.longitude.flatten(),self.latitude.flatten()), self.v_model.flatten(), (SAR_lon,SAR_lat))
        u_eraS = griddata((self.longitude.flatten(),self.latitude.flatten()), self.u_model_shifted.flatten(), (SAR_lon,SAR_lat))
        v_eraS = griddata((self.longitude.flatten(),self.latitude.flatten()), self.v_model_shifted.flatten(), (SAR_lon,SAR_lat))

        fig = plt.figure("MODEL",figsize=(20,20))
        ax = fig.add_subplot(2,2,1,projection=ccrs.PlateCarree())
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        ax.set_title(self.hurricane_name + " " + self.model_name + " zonal component - " + self.time_model,y=1.02)
        im = ax.pcolormesh(self.longitude ,self.latitude ,self.Pb_u_var_before_all.reshape(self.longitude.shape),cmap =plt.cm.Reds, vmin=0,vmax=0.005,alpha=0.5)
        plt.colorbar(im)
        plt.quiver(self.lon[::param],self.lat[::param],u_era[param::,param::],v_era[param::,param::],scale=30, color="green",zorder=1)
        patch_sar = mpatches.Patch(color = 'green',label = self.model_name + ' zonal component')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.xlabels_top = False
        gl.ylabels_right = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.legend(handles=[patch_sar],loc='upper right', fontsize = 20)
        
        ax = fig.add_subplot(2,2,2,projection=ccrs.PlateCarree())
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        im = ax.pcolormesh(self.longitude ,self.latitude ,self.Pb_v_var_before_all.reshape(self.longitude.shape),cmap= plt.cm.Reds,vmin=0,vmax=0.005,alpha=0.5)
        plt.colorbar(im)
        plt.quiver(self.lon[::param],self.lat[::param],u_era[param::,param::],v_era[param::,param::],scale=30, color="green",zorder=1)
        patch_sar = mpatches.Patch(color = 'green',label = self.model_name + ' meridional component')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " " + self.model_name + " meridional component - " + self.time_model,y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right', fontsize = 20)  
        
        ax = fig.add_subplot(2,2,3,projection=ccrs.PlateCarree())
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        im = ax.pcolormesh(self.longitude ,self.latitude ,self.Pb_u_var_init,cmap= plt.cm.Reds,vmin=0,vmax=0.005,alpha=0.5)
        plt.colorbar(im)
        plt.quiver(self.lon[::param],self.lat[::param],u_eraS[param::,param::],v_eraS[param::,param::],scale=30, color="green",zorder=1)
        patch_sar = mpatches.Patch(color = 'green',label = self.model_name + ' shifted zonal component')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.xlabels_top = False
        gl.ylabels_right = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " " + self.model_name + " shifted zonal component - " + self.time_sar,y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right', fontsize = 20) 
        
        ax = fig.add_subplot(2,2,4,projection=ccrs.PlateCarree())
        ax.coastlines(resolution='10m', color='black', linewidth=1)
        im = ax.pcolormesh(self.longitude ,self.latitude ,self.Pb_v_var_init,cmap= plt.cm.Reds,vmin=0,vmax=0.005,alpha=0.5)
        plt.colorbar(im)
        plt.quiver(self.lon[::param],self.lat[::param],u_eraS[param::,param::],v_eraS[param::,param::],scale=30, color="green",zorder=1)
        patch_sar = mpatches.Patch(color = 'green',label = self.model_name + ' shifted meridional component')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " " + self.model_name + " shifted meridional component - " + self.time_sar,y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right', fontsize = 20)
        plt.savefig(self.PATH_+self.hurricane_name+"_"+self.time_sar+"_"+self.model_name+"model_shift_.jpeg")
        
    def model2(self):
        param =20
        
        fig = plt.figure(self.model_name +" shift analysis",figsize=(30,15))
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.windspeed_obs,cmap=plt.cm.binary) ; cbar = plt.colorbar(im) ;
        cbar.ax.set_ylabel("Sar wind speed (m/s)")
                
        ax.quiver(self.lon[::param],self.lat[::param],self.u_sar[::param,::param],self.v_sar[::param,::param],color='b',scale = 35)
        ax.streamplot(self.longitude ,self.latitude ,self.u_model,self.v_model,color = 'green',linewidth=2.3)
        ax.set_title("Orignal model at " + self.time_model, y=1.02)
        blue_patch = mpatches.Patch(color='blue', label='SAR streaks')
        green_patch = mpatches.Patch(color='green', label=self.model_name)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.legend(handles=[green_patch,blue_patch],loc='upper right')
        ax.grid()
    
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.windspeed_obs,cmap=plt.cm.binary) ; cbar = plt.colorbar(im) ;
        cbar.ax.set_ylabel("Sar wind speed (m/s)")
                
        ax.quiver(self.lon[::param],self.lat[::param],self.u_sar[::param,::param],self.v_sar[::param,::param],color='b',scale = 35)
        ax.streamplot(self.longitude ,self.latitude ,self.u_model_shifted,self.v_model_shifted,color = 'green',linewidth=2.3)
        ax.set_title("Shifted model", y=1.02)
        blue_patch = mpatches.Patch(color='blue', label='SAR streaks')
        green_patch = mpatches.Patch(color='green', label=self.model_name)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.legend(handles=[green_patch,blue_patch],loc='upper right')
        ax.grid()
        
        plt.savefig(self.PATH_+self.hurricane_name+"_"+self.time_sar+"_"+self.model_name+"shift_vs_sar_.jpeg")
        
        
        fig = plt.figure(self.model_name +" shift analysis2",figsize=(20,20))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        #im = ax.pcolormesh(self.lon,self.lat,self.windspeed_obs,cmap=plt.cm.binary) ; cbar = plt.colorbar(im) ;
        #cbar.ax.set_ylabel("Sar wind speed (m/s)")
                
        ax.quiver(self.lon[::param],self.lat[::param],self.u_sar[::param,::param],self.v_sar[::param,::param],color='b',scale = 35)
        ax.quiver(self.longitude ,self.latitude ,self.u_model,self.v_model,color = 'black',linewidth=2.3,scale = 35)
        ax.quiver(self.longitude ,self.latitude ,self.u_model_shifted,self.v_model_shifted,color = 'green',linewidth=2.3,scale = 35)

        blue_patch = mpatches.Patch(color='blue', label='SAR directions')
        black_patch = mpatches.Patch(color='black', label=self.model_name + " directions")
        green_patch = mpatches.Patch(color='green', label=self.model_name + " shifted directions")

        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.legend(handles=[blue_patch,black_patch,green_patch],loc='upper right',fontsize = 20)
        ax.grid()

        ax.quiver(self.lon[::param],self.lat[::param],self.u_sar[::param,::param],self.v_sar[::param,::param],color='b',scale = 35)
        ax.set_title(self.model_name + " before and after translation", y=1.02, fontsize = 20)
        plt.savefig(self.PATH_+self.hurricane_name+"_"+self.time_sar+"_"+self.model_name+"shift_vs_sar_2.jpeg")
        
        
        
    def sar(self):
        fig = plt.figure("SAR",figsize=(30,15))
        
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.u_sar); 
        plt.colorbar(im)
        patch_sar = mpatches.Patch(color = 'blue',label = 'SAR original zonal component')
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " Orignal SAR zonal component - " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right')   
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.v_sar); 
        plt.colorbar(im)
        patch_sar = mpatches.Patch(color = 'blue',label = 'SAR original meridional component')
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " Orignal SAR meridional component - " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right')   
        plt.savefig(self.PATH_+self.hurricane_name+"_"+self.time_sar+"_sar_components.jpeg")
        
        param=20
        fig = plt.figure("SAR_winddir",figsize=(20,20))
        ax = ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs,cmap=plt.cm.Reds,vmin = 20, vmax = 70); 
        cbar = plt.colorbar(im) ; cbar.set_label("Sar streaks uncertainty [°]")
        ax.quiver(self.lon[::param],self.lat[::param], \
                  self.u_sar[::param,::param],self.v_sar[::param,::param],color="blue",headwidth =0,scale = 30)
        patch_sar = mpatches.Patch(color = 'blue',label = 'original SAR wind streaks')
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " Orignal SAR wind streaks orientation\nSAR at " + self.time_sar,fontsize = 24, y=1.02)
        ax.legend(handles=[patch_sar],loc='upper right')   
        plt.tight_layout()
        plt.savefig(self.PATH_ +self.hurricane_name+"_"+self.time_sar+"_sar_streaks_stddev.jpeg")
        
        
    def oicp(self):
        fig = plt.figure("OI_components",figsize=(30,15))
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.longitude,self.latitude,self.xa_u); 
        ##ax.contour(self.lon,self.lat,self.windspeed_obs,alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("OI zonal component")
        patch_oi = mpatches.Patch(color = 'red',label = 'OI zonal component')
        patch_sar = mpatches.Patch(color = "black", label = "SAR acquisition")
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " OI zonal component - " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_oi,patch_sar],loc='upper right')   
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.longitude,self.latitude,self.xa_v); 
        #ax.contour(self.lon,self.lat,self.windspeed_obs,alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("OI meriodional component")
        patch_oi = mpatches.Patch(color = 'red',label = 'OI meridional component')
        patch_sar = mpatches.Patch(color = "black", label = "SAR acquisition")
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " OI meridional component - " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_oi,patch_sar],loc='upper right')   
        plt.savefig(self.PATH_ +self.hurricane_name+"_oi_components.jpeg")
        
        
    def oi_speed_in_bckg(self):
        fig = plt.figure("OI spd backup strmplot",figsize=(20,20))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        #ax = plt.axes(projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.windspeed_obs, 
                      cmap = plt.cm.binary)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("Wind speed [m/s]")
     
        ax.streamplot(self.longitude ,self.latitude , \
                      self.xa_u,self.xa_v, \
                      color="red",arrowstyle='->', arrowsize=1.5,density = 0.7)
        
        patch_OI = mpatches.Patch(color = 'red',label = 'OI')  
        ax.streamplot(self.longitude,self.latitude , \
                      self.u_model_shifted,self.v_model_shifted, \
                      color="green",arrowstyle='->', arrowsize=1.5,density = 0.7); 
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        
        param = 25
        ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 30 ); 
        patch_SAR = mpatches.Patch(color = 'blue',label = 'SAR wind streaks')
        ax.set_aspect('equal')
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " comparison SAR - " +self.model_name+ " - OI at " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_SAR,patch_MODEL,patch_OI],loc='upper right')   
        plt.savefig(self.PATH_ +self.hurricane_name+"oi_streamplot_spd_bckgd.jpeg")
        
        param=20
        SAR_lon,SAR_lat = np.meshgrid(self.lon,self.lat)
        u_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_u.flatten(), (SAR_lon,SAR_lat))
        v_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_v.flatten(), (SAR_lon,SAR_lat))

        fig = plt.figure("OI spd backup quiver",figsize=(20,20))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.windspeed_obs, 
                      cmap = plt.cm.binary)
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_[::param,::param],v_[::param,::param],color="red",scale = 30)
        ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.streamplot(self.longitude ,self.latitude , \
              self.u_model_shifted,self.v_model_shifted, \
              color="green"); 
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        patch_SAR = mpatches.Patch(color = 'blue',label = 'SAR wind streaks')
        patch_oi = mpatches.Patch(color = 'red',label = 'OI result ')
        
        ax.set_xlim(SAR_lon.min(),SAR_lon.max())
        ax.set_ylim(SAR_lat.min(),SAR_lat.max())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " comparison SAR - " +self.model_name+ " OI at " + self.time_sar, y=1.02)
        ax.legend(handles=[patch_SAR,patch_MODEL,patch_oi],loc='upper right')   
        plt.savefig(self.PATH_ +self.hurricane_name+"oi_quiver_spd_bckgd.jpeg")

    def oi_stddev_bckgd(self):
        param=20
        SAR_lon,SAR_lat = np.meshgrid(self.lon,self.lat)
        u_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_u.flatten(), (SAR_lon,SAR_lat))
        v_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_v.flatten(), (SAR_lon,SAR_lat))
        u_model= griddata((self.longitude.flatten(),self.latitude.flatten()), self.u_model_shifted.flatten(), (SAR_lon,SAR_lat))
        v_model= griddata((self.longitude.flatten(),self.latitude.flatten()), self.v_model_shifted.flatten(), (SAR_lon,SAR_lat))

        fig = plt.figure("OI oi_stddev_bckgd",figsize=(20,20))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs, 
                      cmap = plt.cm.Reds, vmin = 20, vmax = 70)
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Sar streaks uncertainty [°]",fontsize = 28)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_[::param,::param],v_[::param,::param],color="black",scale = 30,zorder=4)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_model[::param,::param],v_model[::param,::param],color="green",scale = 30 )
        ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 30 ); 
        
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        #ax.streamplot(self.longitude ,self.latitude , \
        #      self.u_model_shifted,self.v_model_shifted, \
        #      color="green"); 
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        patch_SAR = mpatches.Patch(color = 'blue',label = 'SAR wind streaks')
        patch_oi = mpatches.Patch(color = 'black',label = 'OI result')
        
        ax.set_xlim(SAR_lon.min(),SAR_lon.max())
        ax.set_ylim(SAR_lat.min(),SAR_lat.max())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " OI product\n"+ "SAR at " + self.time_sar + "\nModel at " + self.time_model, fontsize = 34, y=1.02)
        ax.legend(handles=[patch_SAR,patch_MODEL,patch_oi],loc='upper right', fontsize = 20)   
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"oi_sttdev_bckgd"+self.res+".jpeg") 
    def oi_stddev_bckgd_specific(self):
        
        param=20
        SAR_lon,SAR_lat = np.meshgrid(self.lon,self.lat)
        u_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_u.flatten(), (SAR_lon,SAR_lat))
        v_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_v.flatten(), (SAR_lon,SAR_lat))
        u_model= griddata((self.longitude.flatten(),self.latitude.flatten()), self.u_model_shifted.flatten(), (SAR_lon,SAR_lat))
        v_model= griddata((self.longitude.flatten(),self.latitude.flatten()), self.v_model_shifted.flatten(), (SAR_lon,SAR_lat))

        fig = plt.figure("OI oi_stddev_bckgd",figsize=(20,20))
        ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs, 
                      cmap = plt.cm.Reds, vmin = 20, vmax = 70)
        cbar = plt.colorbar(im)
        
        if self.setup["full_path_sar"] ==  "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":
            R1 = Rectangle((-88.5, 25.5), 1, 1, 0,color="yellow",linewidth=15, fill=False)
            R2 = Rectangle((-88, 22.5), 2, 1.5, 0,color="yellow",linewidth=15, fill=False)
            R3 = Rectangle((-90, 27), 2, 1.5, 0  ,color="yellow",linewidth=15, fill=False)
            R4 = Rectangle((-88, 28.5), 1, 1, 0  ,color="yellow",linewidth=15, fill=False)

            ax.add_patch(R1)
            #ax.text(-88.5 +0.9, 25.5 +0.9, '1',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R2)
            #ax.text(-88+1.9, 22.5+1.4, '2',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R3)
            #ax.text(-90, 27, '3',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R4)
            #ax.text(-88+0.8, 28.5+0.8, '4',fontsize = 25, color = "black", zorder = 3)

        cbar.ax.set_ylabel("Sar streaks uncertainty [°]",fontsize = 24)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_[::param,::param],v_[::param,::param],color="black",scale = 30,zorder=4)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_model[::param,::param],v_model[::param,::param],color="green",scale = 30 )
        ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 30 ); 
        
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        #ax.streamplot(self.longitude ,self.latitude , \
        #      self.u_model_shifted,self.v_model_shifted, \
        #      color="green"); 
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        patch_SAR = mpatches.Patch(color = 'blue',label = 'SAR wind streaks')
        patch_oi = mpatches.Patch(color = 'black',label = 'OI result')
        
        ax.set_xlim(SAR_lon.min(),SAR_lon.max())
        ax.set_ylim(SAR_lat.min(),SAR_lat.max())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " OI product\n"+ "SAR at " + self.time_sar + "\nModel at " + self.time_model, fontsize = 34, y=1.02)
        ax.legend(handles=[patch_SAR,patch_MODEL,patch_oi],loc='upper right')   
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"oi_sttdev_bckgd2"+self.res+".jpeg") 
                
            
            
        if self.setup["full_path_sar"] ==  "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":

            i= 0 
            for r in [R1,R2,R3,R4] : 
                i+=1
                coords = r.get_bbox().get_points()

                fig = plt.figure("OI oi_stddev_bckgd"+str(i),figsize=(20,20))
                ax = fig.add_subplot(1,1,1,projection=ccrs.PlateCarree())
                im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs, 
                              cmap = plt.cm.Reds, vmin = 20, vmax = 70)
                cbar = plt.colorbar(im)
                cbar.ax.set_ylabel("Sar streaks uncertainty [°]",fontsize = 24)
                ax.quiver(self.lon[::param],self.lat[::param], \
                          u_[::param,::param],v_[::param,::param],color="black",scale = 20 ,zorder=4)
                ax.quiver(self.lon[::param],self.lat[::param], \
                          u_model[::param,::param],v_model[::param,::param],color="green",scale = 20 )
                ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 20); 

                ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

                #ax.streamplot(self.longitude ,self.latitude , \
                #      self.u_model_shifted,self.v_model_shifted, \
                #      color="green"); 
                patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
                patch_SAR = mpatches.Patch(color = 'blue',label = 'SAR wind streaks')
                patch_oi = mpatches.Patch(color = 'black',label = 'OI result')

                ax.set_xlim(coords[0,0],coords[1,0])
                ax.set_ylim(coords[0,1],coords[1,1])
                ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
                gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
                gl.top_labels = False
                gl.right_labels = False
                gl.xformatter = LONGITUDE_FORMATTER
                gl.yformatter = LATITUDE_FORMATTER
                ax.set_title(self.hurricane_name + " OI product\n"+ "SAR at " + self.time_sar + "\nModel at " + self.time_model+"\nArea n°"+str(i), fontsize = 34, y=1.02)
                ax.legend(handles=[patch_SAR,patch_MODEL,patch_oi],loc='upper right', fontsize = 22)   
                plt.tight_layout()
                plt.savefig(self.PATH_+self.hurricane_name+"oi_sttdev_bckgd2_"+str(i)+self.res+".jpeg") 
    def oi_model_bckgd(self):
        fig = plt.figure("OI oi_model_bckgd",figsize=(30,15))
        param = 20
        SAR_lon,SAR_lat = np.meshgrid(self.lon,self.lat)
        u_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_u.flatten(), (SAR_lon,SAR_lat))
        v_= griddata((self.longitude.flatten(),self.latitude.flatten()), self.xa_v.flatten(), (SAR_lon,SAR_lat))
        
        u_era = griddata((self.longitude.flatten(),self.latitude.flatten()), self.u_model_shifted.flatten(), (SAR_lon,SAR_lat))
        v_era = griddata((self.longitude.flatten(),self.latitude.flatten()), self.v_model_shifted.flatten(), (SAR_lon,SAR_lat))

        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.longitude,self.latitude,self.Pb_u_var_before_all, cmap = plt.cm.Reds,vmin= 0, vmax = 0.05)
        if self.setup["full_path_sar"] ==  "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":
            R1 = Rectangle((-88.5, 25.5), 1, 1, 0,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R2 = Rectangle((-88, 22.5), 2, 1.5, 0,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R3 = Rectangle((-90, 27), 2, 1.5, 0  ,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R4 = Rectangle((-88, 28.5), 1, 1, 0  ,color="yellow",linewidth=15, fill=False, alpha=0.7)

            ax.add_patch(R1)
            #ax.text(-88.5 +0.9, 25.5 +0.9, '1',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R2)
            #ax.text(-88+1.9, 22.5+1.4, '2',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R3)
            #ax.text(-90, 27, '3',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R4)
            #ax.text(-88+0.8, 28.5+0.8, '4',fontsize = 25, color = "black", zorder = 3)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("zonal variance on " + self.model_name)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_[::param,::param],v_[::param,::param],color="black",scale = 30 )
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_era[::param,::param],v_era[::param,::param],color="green",scale = 30 )
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        patch_oi = mpatches.Patch(color = 'black',label = 'OI result ')
        
        ax.set_xlim(SAR_lon.min(),SAR_lon.max())
        ax.set_ylim(SAR_lat.min(),SAR_lat.max())
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')

        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " Background variance on zonal component \nModel at " + self.time_model + "\nAnalysis at " + self.time_sar, fontsize = 24, y = 1.02)
        ax.legend(handles=[patch_MODEL,patch_oi],loc='upper right',fontsize = 22)   
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        im = ax.pcolormesh(self.longitude,self.latitude,self.Pb_v_var_before_all, 
                      cmap = plt.cm.Reds,vmin= 0, vmax = 0.05)
        if self.setup["full_path_sar"] ==  "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":
            R1 = Rectangle((-88.5, 25.5), 1, 1, 0,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R2 = Rectangle((-88, 22.5), 2, 1.5, 0,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R3 = Rectangle((-90, 27), 2, 1.5, 0  ,color="yellow",linewidth=15, fill=False, alpha=0.7)
            R4 = Rectangle((-88, 28.5), 1, 1, 0  ,color="yellow",linewidth=15, fill=False, alpha=0.7)

            ax.add_patch(R1)
            #ax.text(-88.5 +0.9, 25.5 +0.9, '1',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R2)
            #ax.text(-88+1.9, 22.5+1.4, '2',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R3)
            #ax.text(-90, 27, '3',fontsize = 25, color = "black", zorder = 3)
            ax.add_patch(R4)
            #ax.text(-88+0.8, 28.5+0.8, '4',fontsize = 25, color = "black", zorder = 3)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("meridional variance on " + self.model_name)
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_[::param,::param],v_[::param,::param],color="black",scale = 30 )
        #ax.quiver(self.lon[::param],self.lat[::param], self.u_sar[::param,::param],self.v_sar[::param,::param], color="blue",headwidth =0,scale = 30 ); 
        #ax.streamplot(self.longitude ,self.latitude , \
        #      self.u_model_shifted,self.v_model_shifted, \
        #      color="green"); 
        ax.quiver(self.lon[::param],self.lat[::param], \
                  u_era[::param,::param],v_era[::param,::param],color="green",scale = 30 )
        patch_MODEL = mpatches.Patch(color = 'green',label = self.model_name)
        patch_oi = mpatches.Patch(color = 'black',label = 'OI result ')
        
        ax.set_xlim(SAR_lon.min(),SAR_lon.max())
        ax.set_ylim(SAR_lat.min(),SAR_lat.max())
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " Background variance on meriodional component \nModel at " + self.time_model + "\nAnalysis at " + self.time_sar, fontsize = 24, y = 1.02)
        ax.legend(handles=[patch_MODEL,patch_oi],loc='upper right',fontsize = 22)  
        
        fig.savefig(self.PATH_+self.hurricane_name+"_OI_Pb_bckgd"+self.res+".jpeg") 
    
    def get_spreads(self):
        fig = plt.figure("OI Pa",figsize=(30,20))

        #Pb_u
        ax = fig.add_subplot(2,3,1,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        im = ax.pcolormesh(self.longitude,self.latitude,self.Pb_u_var_before_all, 
                      cmap = plt.cm.Reds,vmin= 0, vmax = 0.05)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " variance on zonal " + self.model_name + ' - ' + self.time_sar, y=1.02)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("zonal variance on " + self.model_name)
        
        #Pa_u
        ax = fig.add_subplot(2,3,2,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)
        
        param=20
        im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs,cmap=plt.cm.Reds,vmin = 20, vmax = 70); 
        cbar = plt.colorbar(im) ; cbar.set_label("Sar streaks uncertainty [°]")
        ax.quiver(self.lon[::param],self.lat[::param], \
                  self.u_sar[::param,::param],self.v_sar[::param,::param],color="blue",headwidth =0,scale = 30)
        
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " SAR streaks uncertainties"+' - ' + self.time_sar, y=1.02)

        
        #Pa_u
        ax = fig.add_subplot(2,3,3,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        im = ax.pcolormesh(self.longitude,self.latitude,self.Pa_u, 
                      cmap = plt.cm.Reds,vmin= 0, vmax = 0.05)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " variance on analysis zonal component - " + self.time_sar, y=1.02)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("zonal variance of analysis")
        
        #Pb_v
        ax = fig.add_subplot(2,3,4,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        im = ax.pcolormesh(self.longitude,self.latitude,self.Pb_v_var_before_all, 
                      cmap = plt.cm.Reds,vmin= 0, vmax = 0.05)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " variance on meridional " + self.model_name + ' - ' + self.time_sar, y=1.02)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("meridional variance on " + self.model_name)
        
        #SAR
        ax = fig.add_subplot(2,3,5,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        param=20
        im = ax.pcolormesh(self.lon,self.lat,self.stddev_obs,cmap=plt.cm.Reds,vmin = 20, vmax = 70); 
        cbar = plt.colorbar(im) ; cbar.set_label("Sar streaks uncertainty [°]")
        ax.quiver(self.lon[::param],self.lat[::param], \
                  self.u_sar[::param,::param],self.v_sar[::param,::param],color="blue",headwidth =0,scale = 30)

        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " SAR streaks uncertainties"+' - ' + self.time_sar, y=1.02)
        
        
        #Pa_v
        ax = fig.add_subplot(2,3,6,projection=ccrs.PlateCarree())
        #ax.contour(self.lon,self.lat,self.windspeed_obs,[0.01],alpha=0.8, colors="k", linewidths=1)
        ax.plot(*self.sar_polygon.exterior.xy,color = "blue",markersize = 2)

        im = ax.pcolormesh(self.longitude,self.latitude,self.Pa_v, 
                      cmap = plt.cm.Reds,vmin=0,vmax = 0.05)
        
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black')
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels =   False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        ax.set_title(self.hurricane_name + " variance on analysis meridional component - " + self.time_sar, y=1.02)
        cbar = plt.colorbar(im) ; cbar.ax.set_ylabel("meridional spread of analysis")
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"_spreads"+self.res+".jpeg") 
        
        
    def analyse_df_(self):
        df = self.df

        ## P1 
        
        
        mean_d_a_b = np.mean(abs(df["d_a_b"]))
        std_d_a_b = np.std(abs(df["d_a_b"]))
        #print(mean_d_a_b,std_d_a_b)
        mean_d_a_o = np.mean(abs(df["d_a_o"]))
        std_d_a_o = np.std(abs(df["d_a_o"]))
        #print(mean_d_a_o,std_d_a_o)

        mean_d_b_o = np.mean(abs(df["d_b_o"]))
        std_d_b_o = np.std(abs(df["d_b_o"]))
        #print(mean_d_b_o,std_d_b_o)

        fig = plt.figure(figsize = (12,12))
        ax = fig.add_subplot(111)

        Qg = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90)),np.sin(np.radians(mean_d_a_b+90)), color = "green",scale = 3, label = "background mean")
        for i in np.arange(0,int(std_d_a_b),0.8):
            Q = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90+i)),np.sin(np.radians(mean_d_a_b+90+i)), color = "green",scale = 3,headwidth=0)
            Q.set_alpha(0.1)
            Q = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90-i)),np.sin(np.radians(mean_d_a_b+90-i)), color = "green",scale = 3,headwidth=0)
            Q.set_alpha(0.1)

        Qb = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90)),np.sin(np.radians(mean_d_a_o+90)), color = "blue",scale = 3, label = "streaks mean")
        for i in np.arange(0,int(std_d_a_o),1):
            Q = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90+i)),np.sin(np.radians(mean_d_a_o+90+i)), color = "blue",scale = 3,headwidth=0)
            Q.set_alpha(0.05)
            Q = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90-i)),np.sin(np.radians(mean_d_a_o+90-i)), color = "blue",scale = 3,headwidth=0)
            Q.set_alpha(0.05)
        #ax.quiver(0,0,-np.cos(mean_d_b_o),np.sin(mean_d_b_o), color = "b",scale = 2)
        Qr = ax.quiver(0,0,0,1,color = "red",scale = 3, label = "analysis mean")

            
        str_title = self.hurricane_name + "\nmean(|analysis-obs|) = " + str(round(mean_d_a_o,2)) + "°" +"\n" + \
                    "mean(|analysis-background|) = " + str(round(mean_d_a_b,2)) + "°" +"\n" \
                     + "SAR at " + self.time_sar + "\nModel at " + self.time_model

        ax.legend(fontsize = 20)
        ax.set_title(str_title,fontsize = 24, y=1.02)
        ax.set_xticks([])
        ax.set_yticks([])
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"_analyis_OI_vs_inputs1"+self.res+".jpeg") 
        

        
        
        ## P2 
        
        mean_d_a_b_s = []
        std_d_a_b_s = []
        under__a_b_s = []
        over__a_b_s = []
        
        
        mean_d_a_o_s = []
        std_d_a_o_s = []
        under__d_a_o_s = []
        over__d_a_o_s = []
        
        
        mean_d_b_o_s = []
        std_d_b_o_s = []
        under__d_b_o_s = []
        over__d_b_o_s = []

        
        relative_obs = []
        relative_mod = []
        
        ii = np.arange(5,85,1)
        for i in ii:
            curr_df = df[(df.y0_stddev < i+5) & (df.y0_stddev > i-5)]
            
            if len(curr_df) > 0 :
                mean_d_a_b = np.mean(abs(curr_df["d_a_b"]))
                std_d_a_b = np.std(abs(curr_df["d_a_b"]))
                #print(mean_d_a_b,std_d_a_b)
                
                mean_d_a_o = np.mean(abs(curr_df["d_a_o"]))
                std_d_a_o = np.std(abs(curr_df["d_a_o"]))
                #print(mean_d_a_o,std_d_a_o)
        
                mean_d_b_o = np.mean(abs(curr_df["d_b_o"]))
                std_d_b_o = np.std(abs(curr_df["d_b_o"]))
                
                mean_d_a_b_s.append(mean_d_a_b)
                std_d_a_b_s.append(std_d_a_b)
                under__a_b_s.append(mean_d_a_b-std_d_a_b)
                over__a_b_s.append(mean_d_a_b+std_d_a_b)
                    
                mean_d_a_o_s.append(mean_d_a_o)
                std_d_a_o_s.append(std_d_a_o)
                under__d_a_o_s.append(mean_d_a_o-std_d_a_o)
                over__d_a_o_s.append(mean_d_a_o+std_d_a_o)
                
                mean_d_b_o_s.append(mean_d_b_o)
                std_d_b_o_s.append(std_d_b_o)
                under__d_b_o_s.append(mean_d_b_o-std_d_b_o)
                over__d_b_o_s.append(mean_d_b_o+std_d_b_o)
                
                #relative_mod.append(np.min(1.0,mean_d_a_b/mean_d_b_o))
                #relative_obs.append(np.min(1.0,mean_d_a_o/mean_d_b_o))

                
                
            else :
                mean_d_a_b_s.append(np.nan)
                std_d_a_b_s.append(np.nan)
                under__a_b_s.append(np.nan)
                over__a_b_s.append(np.nan)
                
                mean_d_a_o_s.append(np.nan)
                std_d_a_o_s.append(np.nan)
                under__d_a_o_s.append(np.nan)
                over__d_a_o_s.append(np.nan)
        
                mean_d_b_o_s.append(np.nan)
                std_d_b_o_s.append(np.nan)
                under__d_b_o_s.append(np.nan)
                over__d_b_o_s.append(np.nan)
                
                #relative_mod.append(np.nan)
                #relative_obs.append(np.nan)
                
                

        #plt.plot(ii,mean_d_b_o_s)
        plt.figure("didier0",figsize = (12,12))
        plt.plot(ii,mean_d_a_b_s,"g",label="|OI - ERA5|")
        #plt.fill_between((ii),mean_d_a_b_s)
        #plt.fill_between((ii), under__a_b_s, over__a_b_s,
        #                 color='chartreuse', alpha=0.4, label = "std (°)")
        
        plt.plot(ii,mean_d_a_o_s,"r",label="|OI - SAR|")
        #plt.fill_between((ii), under__d_a_o_s, over__d_a_o_s,
        #                 color='lightcoral', alpha=0.4, label = "std (°)")
        
        plt.plot(ii,mean_d_b_o_s,"k",label="|ERA5 - SAR|")

        plt.legend(fontsize = 20)
        plt.title(self.hurricane_name + " Result angle in function of SAR streaks uncertainty \n" + "SAR at " + self.time_sar + "\nModel at " + self.time_model, fontsize = 24)
        plt.xlabel("SAR streaks uncertainty [°]",fontsize = 20)
        plt.ylabel("difference [°]",fontsize = 20)
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"_analyis_OI_vs_inputs2"+self.res+".jpeg") 
        
        
        """
            
        plt.figure("didier",figsize = (12,12))
        plt.plot(ii,relative_mod,"g",label="relative contribution of " + self.model_name)
        #plt.fill_between((ii), under__a_b_s, over__a_b_s,
        #                 color='chartreuse', alpha=0.4, label = "std (°)")
        
        #plt.plot(ii,relative_obs,"r",label="relative contribution of SAR")
        #plt.fill_between((ii), under__d_a_o_s, over__d_a_o_s,
        #                 color='lightcoral', alpha=0.4, label = "std (°)")
        

        #plt.legend(fontsize = 20)
        plt.title(self.hurricane_name + " Result angle in function of SAR streaks uncertainty \n" + "SAR at " + self.time_sar + "\nModel at " + self.time_model, fontsize = 24)
        plt.xlabel("SAR streaks uncertainty [°]",fontsize = 20)
        plt.ylabel("difference [°]",fontsize = 20)
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"_analyis_OI_vs_inputs4"+self.res+".jpeg") 
        
        """
        
        ## 3
        
        figF = plt.figure(figsize = (24,12))
        ax = figF.add_subplot(121)
        Qg = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90)),np.sin(np.radians(mean_d_a_b+90)), color = "green",scale = 3, label = "background mean")
        for i in np.arange(0,int(std_d_a_b),0.8):
            Q = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90+i)),np.sin(np.radians(mean_d_a_b+90+i)), color = "green",scale = 3,headwidth=0)
            Q.set_alpha(0.1)
            Q = ax.quiver(0,0,-np.cos(np.radians(mean_d_a_b+90-i)),np.sin(np.radians(mean_d_a_b+90-i)), color = "green",scale = 3,headwidth=0)
            Q.set_alpha(0.1)

        Qb = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90)),np.sin(np.radians(mean_d_a_o+90)), color = "blue",scale = 3, label = "streaks mean")
        for i in np.arange(0,int(std_d_a_o),1):
            Q = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90+i)),np.sin(np.radians(mean_d_a_o+90+i)), color = "blue",scale = 3,headwidth=0)
            Q.set_alpha(0.05)
            Q = ax.quiver(0,0,np.cos(np.radians(mean_d_a_o+90-i)),np.sin(np.radians(mean_d_a_o+90-i)), color = "blue",scale = 3,headwidth=0)
            Q.set_alpha(0.05)
        #ax.quiver(0,0,-np.cos(mean_d_b_o),np.sin(mean_d_b_o), color = "b",scale = 2)
        Qr = ax.quiver(0,0,0,1,color = "red",scale = 3, label = "analysis mean")

            
        str_title = self.hurricane_name + "\nmean(|analysis-obs|) = " + str(round(mean_d_a_o,2)) + "°" +"\n" + \
                    "mean(|analysis-background|) = " + str(round(mean_d_a_b,2)) + "°" +"\n" \
                     + "SAR at " + self.time_sar + "\nModel at " + self.time_model

        ax.legend(fontsize = 17)
        ax.set_title(str_title,fontsize = 17, y=1.02)
        ax.set_xticks([])
        ax.set_yticks([])
        
        ax = figF.add_subplot(122)
        ax.plot(ii,mean_d_a_b_s,"g",label="|analysis - model|")
        #plt.fill_between((ii), under__a_b_s, over__a_b_s,
        #                 color='chartreuse', alpha=0.4, label = "std (°)")
        
        ax.plot(ii,mean_d_a_o_s,"r",label="|analysis - obs|")
        #plt.fill_between((ii), under__d_a_o_s, over__d_a_o_s,
        #                 color='lightcoral', alpha=0.4, label = "std (°)")
        
        ax.plot(ii,mean_d_b_o_s,"k",label="|model - obs|")

        ax.legend(fontsize = 17)
        ax.set_title(self.hurricane_name + " Result angle in function of SAR streaks uncertainty \n" + "SAR at " + self.time_sar + "\nModel at " + self.time_model, fontsize = 17)
        ax.set_xlabel("SAR streaks uncertainty [°]",fontsize = 17)
        ax.set_ylabel("difference(°)",fontsize = 17)
        plt.tight_layout()
        plt.savefig(self.PATH_+self.hurricane_name+"_analyis_OI_vs_inputs_final"+self.res+".jpeg") 



if __name__ =='__main__':           
    #Parameters in input
    choices_model = ["era5","mars"]
    choices_translations = ["0 : no translation of model",
                            "1 : shift dist.eucl",
                            "2 : shift dist.angulaire",
                            "3 : member shift by center (manual)"]
    
    choices_R = ["0 : sin(streaks_dir_uncertainty)",
                 "1 : constant"]
    
    choices_Desroziers = ["True : apply Desroziers",
                          "False : do not apply Desroziers"]
    
    choices_uv = ["True : u,v treated together",
                 "False : u,v treated separately"]
        
    choices_loc = ["True : apply Localisation",
                   "False : do not apply Localisation"]
    import argparse
    parser = argparse.ArgumentParser(description = 'run oi')
    parser.add_argument('--inputfile',default = True,help='choose the input L2-IFREMER filename')
    parser.add_argument('--res',default = "0.16",help='choose the final product grid spacing')
    parser.add_argument('--model_name',default = "era5",choices = choices_model, help= 'choose between %s'%(choices_model))
    parser.add_argument('--uv', default = True, choices = choices_uv, help= 'choose between %s'%(choices_uv))
    parser.add_argument('--lcz',default = True, choices = choices_loc,  help= 'choose between %s'%(choices_loc))
    parser.add_argument('--c', help= 'choose the localization distance parameter [km]')
    parser.add_argument('--desroziers', choices = choices_Desroziers, help= 'choose between %s'%(choices_Desroziers))
    parser.add_argument('--translation_method', choices = choices_translations,default = 0, help= 'choose between %s'%(choices_translations))
    parser.add_argument('--R_function_method',choices_R,default = 0, help= 'choose between %s'%(choices_R))
    parser.add_argument('--localization_method',default = 0, help = "0")

    args = parser.parse_args()


    filename = args.inputfile.split("/")[-1]
    year = filename.split('-')[4][0:4]

    setup = {
        "res" : float(args.res),
        "model_name" : args.model_name,
        "localization" : eval(args.lcz),
        "filename" : filename,
        "full_path_sar" : args.inputfile,
        "uv_together" : eval(args.uv),
        "year" : year,
        "c" : int(args.c),
        "desroziers" : eval(args.desroziers),
        
        "localization_method" : int(args.localization_method),
        "R_function_method": int(args.R_function_method),
        "translation_method" : int(args.translation_method)
    }   

    lcz = "__lcz" if eval(args.lcz) else "nolcz"
    uv_ = "tog" if eval(args.uv) else "sep"
    setup["output_fn"] = getOutput(path_saving,setup,"","_RECALAGE_"+str(setup["translation_method"])+"c_"+args.c)
    ## GET hurricane name
    try : 
        urll = "https://cyclobs.ifremer.fr/app/api/getData?instrument=C-Band_SAR&include_cols=data_url,cyclone_name,acquisition_start_time"
        df=pd.read_csv(urll).sort_values(by='acquisition_start_time').reset_index().drop('index',axis=1)
        df["data_url"] = df["data_url"].apply(lambda x: x.split('/')[-1]) 
        setup["hurricane_name"] = df[df.data_url == filename].cyclone_name.values[0]
    except : 
        print("Can't associate hurricane name")
        
        hurricane_name = ""
        if args.inputfile == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc" :
             hurricane_name = "MICHAEL"
        elif args.inputfile == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2020/303/S1A_EW_OWIM_CC_20201029T205631_20201029T205758_035016_0415BC/post_processing/nclight_L2M/s1a-ew-owi-cm-20201029t205631-20201029t205758-000003-0415BC_ll_gd.nc":
            hurricane_name = "GONI"
        print(hurricane_name)
        setup["hurricane_name"] = hurricane_name
    print("hurricane_name is " + setup["hurricane_name"] )

    analyse = ANALYSE(setup)   
    analyse.createFolder()
    analyse.displays_covs();
    #analyse.model_shift_effect();
    analyse.model2();
    analyse.oicp();
    analyse.sar();
    analyse.oi_speed_in_bckg();
    analyse.oi_stddev_bckgd();
    analyse.oi_stddev_bckgd_specific();

    analyse.oi_model_bckgd();
    analyse.get_spreads();
    
    analyse.analyse_df_();
    print("analyses done and saved")
