#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 14:38:12 2021

@author: vincelhx
"""
import numpy as np

def getDistanceMatrix(LON_dec,LAT_dec):    
    """
    Get distance matrix : matrix of distances between points of the covariance matrix

    Parameters
    ----------
    LON_dec : array()
        array of longitudes.
    LAT_dec : array()
        array of latiitudes.

    Returns
    -------
    array()
        array of distances.

    """
    from sklearn.metrics.pairwise import haversine_distances
    if (np.ndim(LON_dec) != 1) :
        _lons = LON_dec.flatten()
    else :
        _lons = LON_dec
        
    if (np.ndim(LAT_dec) != 1) :
        _lats = LAT_dec.flatten()    
    else :
        _lats = LAT_dec
        
    coords = np.array([(np.radians(_lats[i]),np.radians(_lons[i])) for i in range(len(_lons))])
    return haversine_distances(coords,coords) * 6371 

def isSymetric(a, tol=1e-8):
    return np.all(np.abs(a-a.T) < tol)


def getLocalised_B(B, loc, nb_state_variables):
    """
    Get the localised model covariance matrix

    Parameters
    ----------
    B : array
        model covariance matrix.
    loc : array
        localisation matrix (e.g. exp(-d/c))
    nb_state_variables : int
        number of variables in the state (2 if zonal and meridional component used).

    Returns
    -------
    newB : array
        localised covariance matrix.
    """

    newB = np.empty_like(B)
    if nb_state_variables == 2:
        Pb_u = B[0:B.shape[0]//2, 0:B.shape[0]//2]
        Pb_uv = B[0:B.shape[0]//2, B.shape[0]//2:]
        Pb_v = B[B.shape[0]//2:, B.shape[0]//2:]
        Pb_vu = B[B.shape[0]//2:, 0:B.shape[0]//2]
        
        Pb_uLocalized = loc*Pb_u
        Pb_vLocalized = loc*Pb_v
        Pb_uvLocalized = loc*Pb_uv
        Pb_vuLocalized = loc*Pb_vu
        
        newB[0:B.shape[0]//2, 0:B.shape[0]//2] = Pb_uLocalized
        newB[B.shape[0]//2:, B.shape[0]//2:] = Pb_vLocalized
        newB[0:B.shape[0]//2, B.shape[0]//2:] = Pb_uvLocalized
        newB[B.shape[0]//2:, 0:B.shape[0]//2] = Pb_vuLocalized

    elif nb_state_variables == 1:
        newB = loc*B
        
    return newB

def f_sin(stddev):
    return np.sin(stddev)

"""
def build_Pb(d,Pb,spread_excepted,one_param):
    newB = np.zeros_like(Pb)
    Pb_u = Pb[0:int(Pb.shape[0]/2),0:int(Pb.shape[0]/2)]
    Pb_v = Pb[int(Pb.shape[0]/2):,int(Pb.shape[0]/2):]
    Pb_uv = Pb[0:int(Pb.shape[0]/2),int(Pb.shape[0]/2):]

    ddd = d.copy()
    #ddd[np.tril_indices(ddd.shape[0], -1)] = np.nan
    couples = [(i,i+50) for i in np.arange(0,(np.nanmax(ddd)//15+1)*15,5)]
    absc = np.array([(couple[0]+couple[1])/2 for couple in couples]) 
    #print(absc[(absc<500) & (absc > 100)])

    
    newB[0:int(Pb.shape[0]/2),0:int(Pb.shape[0]/2)] = build_sous_matrice(d,Pb_u,absc,couples,spread_excepted,one_param)
    newB[int(Pb.shape[0]/2):,int(Pb.shape[0]/2):] = build_sous_matrice(d,Pb_v,absc,couples,spread_excepted,one_param)
    newB[0:int(Pb.shape[0]/2),int(Pb.shape[0]/2):] = build_sous_matrice(d,Pb_uv,absc,couples,spread_excepted,one_param)
    newB[int(Pb.shape[0]/2):,0:int(Pb.shape[0]/2)] = newB[0:int(Pb.shape[0]/2),int(Pb.shape[0]/2):].T
    return newB

def build_sous_matrice(d,sous_matrice,absc,couples,spread_excepted,one_param):
    new_sous_matrice = np.zeros_like(sous_matrice)
    
    for i in range(len(np.diag(sous_matrice))):
    #for i in range(0,20):
        
        d_ = d[i,:]
        b_ = sous_matrice[i,:]
        #print(d[i,:], Pb_u[i,:])
        
        #print(couples)
        Ls_u = []
        for couple in couples:
            mask = np.where((d_>couple[0]) & (d_<couple[1]))
            Ls_u.append(np.nanmean(np.abs((b_[mask]))))
        

        
        sigma = [1/(len(absc)-(i)) for i in range(len(absc[(absc<500)]))]
        #print(len(sigma))
        
        #print(sigma)
        #x, y = d_[d_<500],np.array(b_)[d_<500]
        #x, y = d_,np.array(b_)
        if one_param :
            a = np.abs(b_[i])
            x, y = absc[(absc<500) & (absc>100)],np.array(Ls_u)[(absc<500) & (absc>100)]
            #print(x,y)
            #print(d_[i])

            popt, __ = curve_fit((lambda x, D: a * np.exp(-x/D)), x, y,bounds=(10,500)) 
            D  = popt
                
            #print(b_[i], D)
            
            print('y = %.3f * exp(-x/%.1f) + 0' % (a, D))
            #plt.plot(d_,0.03*np.exp(-d_/200),"g.")
        else : 
            x, y = absc[(absc<500) & (absc>100)],np.array(Ls_u)[(absc<500) & (absc>100)]
            #print(x,y)
            popt, __ = curve_fit(objective, x, y, bounds = [(-0.1,0),(0.1,500)])
            a, D  = popt
        #print(x,y)

            print('y = %.3f * exp(-x/%.1f) + 0' % (a, D))
            #plt.plot(d_,0.03*np.exp(-d_/200),"g.")
        
        new_sous_matrice[i,i:] = a*np.exp((-d_/D))[i:]
        #print(new_sous_matrice)
       
        plt.figure()
        plt.plot(d_, b_,"k.",label ="true data")
        plt.plot(absc,Ls_u,"r.",label = "modified data for fit")
        plt.plot(x,y,"g.",label="pts used for fit")
        plt.plot(d_,a*np.exp((-d_/D)),"b.",label = 'y = %.3f * exp(-x/%.1f) + 0' % (a, D))
        plt.legend()

    z = new_sous_matrice + new_sous_matrice.T
    np.fill_diagonal(z,np.diag(z)/2)
    
    return z



### Matrices possibles pour définir R

def f_lineaire1(stddev,d):
    return ((2*d)/90)*stddev 



def f_sin2(stddev) :
    if np.degrees(stddev) <= 45 :
        return np.sin(stddev)
    
    else : 
        return 2*np.sin(stddev)
def f_x(x,puiss):
    return x**puiss / (90**puiss)


def f_sigmoid_(stddev):
    if stddev != 0 : 
      return 1 / (1 + np.exp(-stddev))
    else : 
        return 0
    
def f_tanh(stddev):
    if stddev >50 : 
      return np.tanh(np.radians(stddev-50)) 
    else : 
        return 0.012

def f_tanh2(stddev):
    if stddev >35 : 
      return np.tanh(np.radians(stddev-35)) 
    else : 
        return 0.012
    
def f_tanh3(stddev):
    if stddev >25 : 
      return np.tanh(np.radians(stddev-25)) 
    else : 
        return 0.012
    
def _fn(stddev):
    return ((1/90**2)*stddev**2)*stddev/90  
"""