#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 14:01:51 2021

@author: Vincent LHEUREUX
IFREMER    
"""
# Imports

from sar import SAR
from era import ERA
from mars import MARS
from shift import SHIFT_all_members
from oi import OI,desroziers
from save import ncfile
# from members__ import MembersAnalysis
import warnings
warnings.filterwarnings("ignore")
import numpy as np;
import os 
from covariances_manipulation import getDistanceMatrix, getLocalised_B, f_sin
from tools import getOutput,save_associated_df



### PATHs
global_folder = "/home/datawork-cersat-public/project/mpc-sentinel1/analysis/s1_data_analysis/oi/final2_"
path_saving = global_folder +"/results/"
    
main_PATH = '/home1/datawork/vlheureu/'
PATH_SARFILES = "/home1/datawork/vlheureu/DATA/SAR/"


def run(setup):
    """
    Parameters
    ----------
    setup : object
        parameters of the OI process.

    Returns
    -------
    bool
        True if success, False if failed.

    """
    model_name = setup["model_name"]
    res = setup["res"]
    localization = setup["localization"]
    uv_together = setup["uv_together"]
    c = setup["c"]
    translation_method = setup["translation_method"]
    B_covs_to_save = {}
    # getting sar data
    print("Getting sar files...")
    sar = SAR(setup["full_path_sar"])
    time_sar = sar.time_obs;
    if (sar.load(res) == False) : 
        print("No data is sar file")
        #return False
    print("Getting sar files... done")
    
    # loading choosen model
    print("Getting " + model_name + " data")
    if (model_name == "era5") :
        try :
            model = ERA(sar)
            model.load(res)
            print(time_sar,model.time_model)
            print("Getting " + model_name + " data... done")
        except Exception as e: 
            print("- Problem : can't load " + model_name , e)
            return False 
    elif (model_name == "mars") : 
        try :
            model = MARS(sar)
            model.load(res)
            print(time_sar,model.time_model)
            print("Getting " + model_name + " data... done")
        except Exception as e: 
            print("- Problem : can't load " + model_name , e)
            #return False x²x²
    else :
        print("Invalid model : " + model_name)
        #return False
    
    # Shifting the model
    print("Model translation... with method " + str(translation_method))
    shift = SHIFT_all_members(model,sar,True)
    if translation_method == 0:
        print("no translation... done")
        xb_u_after,xb_v_after,u_members_after,v_members_after,yo_u,yo_v,yo_stddev = shift.get_u_v_after(0,0,res)
        xb_u_before = xb_u_after
        xb_v_before = xb_v_after 
    elif translation_method == 1 :
        i_min,j_min,D  = shift.find_best_recalage(res)
        xb_u_after,xb_v_after,u_members_after,v_members_after,yo_u,yo_v,yo_stddev = shift.get_u_v_after(i_min,j_min,res)
        print("model translation... dist.eucl. done")
    elif translation_method == 2 :
        print("model translation... dist.angulaire.. not implemented, \nsee tests_shift.py")
        return;
    elif translation_method == 3 : 
        #ne marche que avec des cyclones GONI et MICHAEL à rés 0.25 ° || 0.16°"
        print("this model translation is working only with Goni and Michael yet at 0.16° or 0.25° resolution... need automation to find center instead of manual center")    
        xb_u_after,xb_v_after,u_members_after,v_members_after,yo_u,yo_v,yo_stddev = shift.shift_from_Gamma1(res, setup["full_path_sar"])
        xb_u_before,xb_v_before,u_members_,v_members_,_3,_4,_5 = shift.get_u_v_after(0,0,res)
        B_covs_to_save["Pb_before_all"] = np.cov(np.append(u_members_,v_members_,axis=0)) ;
        
    print("model translation... done")    
    
    yo_u = yo_u.flatten()
    yo_v = yo_v.flatten()
    yo_stddev = yo_stddev.flatten()

    #mb_analysis = MembersAnalysis(shift,Pb_u,Pb_v)
    #mb_analysis.DisplaySpread();
    #mb_analysis.DisplayOnePt();
    
    if uv_together : 
        nb_state_variables = 2    
        yo = np.append(yo_u,yo_v) ;
        xb = np.append(xb_u_after,xb_v_after) ; 
        print("calculating model variance matrix...")
        # Covariance matrices
        R = np.eye(len(yo[np.isfinite(yo)])) ;
        if setup["R_function_method"] == 0:  
            if "rs2" not in sar.fn: 
                yo_std = np.append(yo_stddev,yo_stddev) ;    
                R = yo_std[np.isfinite(yo_std)]
                diag_after_function = np.array([f_sin(np.radians(i)) for i in yo_std[np.isfinite(yo_std)]])
                R = np.diag(diag_after_function)
        else: 
            print("Method %d for R definition unknown" % (str(setup["R_function_method"])) )
            return
        
        Pb_init = np.cov(np.append(u_members_after,v_members_after,axis=0)) ; 
        B_covs_to_save["Pb_init"] = Pb_init


        if (localization) : 
            # Localization 
            localization_method = setup["localization_method"]
            if localization_method == 0 : 
                print("Getting distance matrix...")
                loc = np.exp(-getDistanceMatrix(shift.LON_dec,shift.LAT_dec)/c)
                print("Getting distance matrix... done")
                print("Applying localisation with Schur product...")
                Pb = getLocalised_B(Pb_init, loc, nb_state_variables)
                B_covs_to_save["Pb_localized"] = Pb
                print("Applying localisation with Schur product... done")
            else :
                print("Method %d unknown" % (localization_method) )
                return
            
        else : 
            # NO Localization            
            Pb = Pb_init
            
        print("calculating covariance matrix... done")

        if setup["desroziers"] : 
            print("oi & Desroziers...")
            xa, Pa_OI ,R , alphas, sigma_o2s, newB = desroziers(yo, R, xb.flatten(), Pb, 0, 10)
            B_covs_to_save["Pb_Desroziers"] = newB

            print("oi & Desroziers... done")
        else:
            print("oi...")
            xa, Pa_OI, d_o_b, d_o_a, H, p, y  = OI(yo, R, xb, Pb, 0)
            print("oi... done")
            alphas = []
            sigma_o2s = []
            
        xa_u = xa[0:int(xa.shape[0]/2)]
        xa_v = xa[int(xa.shape[0]/2):]
        Pa_OI_u = Pa_OI[0:int(Pa_OI.shape[0]/2)]
        Pa_OI_v = Pa_OI[int(Pa_OI.shape[0]/2):]
    
    """
    else : 
        
        // incomplete since unused
        
        
        nb_state_variables = 1 
        Ru = np.eye(len(yo_u[np.isfinite(yo_u)])) # observation covariance matrice
        Rv = np.eye(len(yo_v[np.isfinite(yo_v)])) # observation covariance matrice
        Pb_u = np.cov(u_members_after)
        Pb_v = np.cov(v_members_after)
        spread_u = np.diag(Pb_u).reshape(shift.LON_dec.shape)
        spread_v = np.diag(Pb_v).reshape(shift.LON_dec.shape) 
        if (localization) :       
            print("Getting distance matrix...")
            localz = Localization(shift.LON_dec,shift.LAT_dec)
            d = localz.getDist();
            loc = np.exp(-d/c)
            localiz = True
            print("Getting distance matrix... done")
            print("oi & Desroziers...")
            xa_u,Pa_OI_u,R_u,alpha_u = desroziers(yo_u, Ru, xb_u_after.flatten(), Pb_u, 0, 6,localiz,loc,False)
            xa_v,Pa_OI_v,R_v,alpha_v = desroziers(yo_v, Rv, xb_v_after.flatten(), Pb_v, 0, 6,localiz,loc,False)
            print("oi & Desroziers... done")
        else : 
            print("NO localization...")
            print("oi & Desroziers...")
            xa_u,Pa_OI_u,R_u,alpha_u = desroziers(yo_u, Ru, xb_u_after.flatten(), Pb_u, 0, 6)
            xa_v,Pa_OI_v,R_v,alpha_v = desroziers(yo_v, Rv, xb_v_after.flatten(), Pb_v, 0, 6)
            print("oi & Desroziers... done")
    """
    
    
    
    setup["output_fn"] = output_fn
    setup["time_sar"] = time_sar
    setup["time_model"] = model.time_model

    setup["sigma_o2s"] = sigma_o2s
    setup["alphas"] = alphas
    setup["B_covs"] = B_covs_to_save
    setup["R_diag"] = np.diag(R)
    
    path_df = save_associated_df(xa_u,xa_v, yo_u,yo_v,yo_stddev,xb_u_after,xb_v_after,shift.LON_dec,shift.LAT_dec,setup)
    setup["path_df"] = path_df
    ncfile(shift,xa_u,xa_v,Pa_OI_u,Pa_OI_v,setup,xb_u_before,xb_v_before)
    return True


if __name__ =='__main__':
    #Parameters in input
    choices_model = ["era5","mars"]
    choices_translations = ["0 : no translation of model",
                            "1 : shift dist.eucl",
                            "2 : shift dist.angulaire",
                            "3 : member shift by center (manual)"]
    
    choices_R = ["0 : sin(streaks_dir_uncertainty)",
                 "1 : constant"]
    
    choices_Desroziers = ["True : apply Desroziers",
                          "False : do not apply Desroziers"]
    
    choices_uv = ["True : u,v treated together",
                 "False : u,v treated separately"]
        
    choices_loc = ["True : apply Localisation",
                   "False : do not apply Localisation"]
    import argparse
    parser = argparse.ArgumentParser(description = 'run oi')
    parser.add_argument('--inputfile',default = True,help='choose the input L2-IFREMER filename')
    parser.add_argument('--res',default = "0.16",help='choose the final product grid spacing')
    parser.add_argument('--model_name',default = "era5",choices = choices_model, help= 'choose between %s'%(choices_model))
    parser.add_argument('--uv', default = True, choices = choices_uv, help= 'choose between %s'%(choices_uv))
    parser.add_argument('--lcz',default = True, choices = choices_loc,  help= 'choose between %s'%(choices_loc))
    parser.add_argument('--c', help= 'choose the localization distance parameter [km]')
    parser.add_argument('--desroziers', choices = choices_Desroziers, help= 'choose between %s'%(choices_Desroziers))
    parser.add_argument('--translation_method', choices = choices_translations,default = 0, help= 'choose between %s'%(choices_translations))
    parser.add_argument('--R_function_method',choices_R,default = 0, help= 'choose between %s'%(choices_R))
    parser.add_argument('--localization_method',default = 0, help = "0")

    args = parser.parse_args()

    filename = args.inputfile.split("/")[-1]

    year = filename.split('-')[4][0:4]
    
    """
    hurricane_name = ""
    if args.inputfile == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc" :
             hurricane_name = "MICHAEL"
    elif args.inputfile == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2020/303/S1A_EW_OWIM_CC_20201029T205631_20201029T205758_035016_0415BC/post_processing/nclight_L2M/s1a-ew-owi-cm-20201029t205631-20201029t205758-000003-0415BC_ll_gd.nc":
        hurricane_name = "GONI"
                
    print(hurricane_name)
    """
    setup = {
        "res" : float(args.res),
        "model_name" : args.model_name,
        "localization" : eval(args.lcz),
        "filename" : filename,
        "full_path_sar" : args.inputfile,
        "uv_together" : eval(args.uv),
        "year" : year,
        "c" : int(args.c),
        "desroziers" : eval(args.desroziers),
        "localization_method" : int(args.localization_method),
        "R_function_method": int(args.R_function_method),
        "translation_method" : int(args.translation_method)
    }   
            
    output_fn = getOutput(path_saving,setup,"","_RECALAGE_"+str(setup["translation_method"])+"c_"+args.c)
    
    #Do not treat if already treated    

    if (os.path.isfile(output_fn)) :
        print(filename, "ALREADY TREATED at res = ", args.res)
    else :
        print(filename,"START")
        if (run(setup)) : 
            print(filename, "END") 
        else : 
            print("Echec -  " + filename)