 #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 13:02:24 2021

@author: vincelhx
"""

import datetime
import numpy as np 
import xarray as xr
PATH_mars_members = "/home/datawork-cersat-public/project/mpc-sentinel1/data/ancillary/mars/"
from scipy.interpolate import griddata

class MARS(object):
    def __init__(self,obs):
        self.obs = obs
        self.time_obs = obs.time_obs 
        self.name = "MARS"
        
        
    def getTime(self):
        """
        Function has to be checked
        Returns
        -------
        TYPE
            self.time_model : time of the model.
        """
        limit_date_MARS_hourly = datetime.datetime(2016,11,22,0,0,0)
        limit_date_MARS_time_6h = datetime.datetime(2015,6,22,0,0,0)
        temps_sar = self.time_obs.split(' - ')[1]
        sar_date_product_date = (datetime.datetime(int(self.time_obs[0:4]),int(self.time_obs[5:7]),int(self.time_obs[8:10]),
                                int(temps_sar[0:2]),int(temps_sar[3:5]),int(temps_sar[6:8])))
        
        
        unixSAR = (sar_date_product_date- - datetime.datetime(1970,1,1)).total_seconds()


        delta_hours_model = 1
        if sar_date_product_date <= limit_date_MARS_hourly : 
            delta_hours_model = 3

        if unixSAR % (delta_hours_model*60*60) <=  (delta_hours_model)*60*60 :
            ts = int(unixSAR-unixSAR % (delta_hours_model*60*60))
            self.time = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d - %H:%M:%S UTC')
        else : 
            ts = int(unixSAR + (delta_hours_model*60*60  -unixSAR % (delta_hours_model*60*60)))
            self.time = datetime.datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d - %H:%M:%S UTC')
                       
        return self.time
    def getMembers(self):
        """
        Returns
        -------
        TYPE
            self.members_nc : netCDF4 file of the model members
        """  
        time = self.getTime()
        date_mars = time.split(' - ')[0]
        hh = time[13:15]
        step = str(int(hh)-int(hh)%6)
        #yday = datetime.date(int(date_mars[0:4]),int(date_mars[5:7]),int(date_mars[8:10])).timetuple().tm_yday
        print(PATH_mars_members+"/MARS_"+date_mars.replace('-','')+"_"+hh+"model"+step+".nc")
        self.ds_members = xr.open_dataset(PATH_mars_members+"/MARS_"+date_mars.replace('-','')+"_"+hh+"_model"+step+".nc",'a')
        return self.ds_members
    
    def checkTime(self):
        time_nc = self.nc["time"][:][0]*60*60
        dt1970 = datetime.datetime(1970,1,1,0,0,tzinfo=datetime.timezone.utc)
        dt1900 = datetime.datetime(1900,1,1,0,0,tzinfo=datetime.timezone.utc)
        time_delta = (dt1970-dt1900).total_seconds()
        md= (time_nc-time_delta)
        print(datetime.datetime.utcfromtimestamp(md).strftime('%Y-%m-%d %H:%M:%S'))
        
    def load(self,res):
        """
        Function has to be checked
        Parameters
        ----------
        res : float
            grid resolution.

        Returns
        -------
        None
        """      
        ds_members = self.getMembers()

        lons = np.array(ds_members['longitude'])
        lats = np.flip(np.array(ds_members['latitude']))
        LON,LAT = np.meshgrid(lons,lats)
        u_10 = np.array(ds_members['u10'])[0]
        v_10 = np.array(ds_members['v10'])[0]
        # means of members
        u_mean = np.flip(np.mean(u_10,axis=0),axis=0)
        v_mean = np.flip(np.mean(v_10,axis=0),axis=0)
        spd = np.sqrt(np.sqrt(u_mean**2+v_mean**2))
        
        index_lat_min  = np.argmin(np.abs(lats-self.sar.grid_lat_min))-10
        index_lat_max  = np.argmin(np.abs(lats-self.sar.grid_lat_max))+10
        index_lon_min  = np.argmin(np.abs(lons-self.sar.grid_lon_min))-10
        index_lon_max  = np.argmin(np.abs(lons-self.sar.grid_lon_max))+10
        lonmem = lons[index_lon_min:index_lon_max]
        latmem = lats[index_lat_min:index_lat_max]   
        LONmem,LATmem = np.meshgrid(lonmem,latmem)
        u_mean = u_mean[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
        v_mean = v_mean[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
        spd = spd[index_lat_min:index_lat_max,index_lon_min:index_lon_max]
        
        
        ### interpolation 
        ## mean
        lon_grid = np.arange(self.sar.grid_lon_min,self.sar.grid_lon_max,res)
        lat_grid = np.arange(self.sar.grid_lat_min,self.sar.grid_lat_max,res)
        pts = np.array([LONmem.flatten(),LATmem.flatten()]).T # pts décrivent current grid  
        xb_u = griddata(pts,u_mean.flatten(),(self.sar.LON_grid,self.sar.LAT_grid),method="linear")
        xb_v = griddata(pts,v_mean.flatten(),(self.sar.LON_grid,self.sar.LAT_grid),method="linear")
        self.spd_mean = griddata(pts,spd.flatten(),(self.obs.LON_grid,self.obs.LAT_grid),method="linear")

        self.xb_u = xb_u/self.spd_mean
        self.xb_v = xb_v/self.spd_mean    
        
        ## members
        self.members_u = np.empty(shape=(len(lon_grid)*len(lat_grid),50))
        self.members_v = np.empty(shape=(len(lon_grid)*len(lat_grid),50))
        
        #TO MODIF
        for i in range (0,50):
            u_10i = np.flip(u_10[i][:],axis=0)
            v_10i = np.flip(v_10[i][:],axis=0)
            spdi = (np.sqrt(u_10i**2+v_10i**2))
    
            u_i = griddata(pts,u_10i[index_lat_min:index_lat_max,index_lon_min:index_lon_max].flatten(),(self.sar.LON_grid,self.sar.LAT_grid).flatten(),method="linear")
            v_i = griddata(pts,v_10i[index_lat_min:index_lat_max,index_lon_min:index_lon_max].flatten(),(self.sar.LON_grid,self.sar.LAT_grid).flatten(),method="linear")
            spd_i = griddata(pts,spdi[index_lat_min:index_lat_max,index_lon_min:index_lon_max].flatten(),(self.sar.LON_grid,self.sar.LAT_grid).flatten(),method="linear")
            
            self.members_u[:,i] = u_i/spd_i
            self.members_v[:,i] = v_i/spd_i
        
