#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 08:01:32 2021

@author: vincelhx
"""
import numpy as np
from AnDA_stat_functions import inv_using_SVD

def OI(yo, R, xb, B, begin_obs) :
    """
    Parameters
    ----------
    yo : array(flaot)
        observation vector.
    R : array(flaot)
        observation covariance matrix.
    xb : array(flaot)
        backgroud vector.
    B : array(flaot)
        backgroud covariance matrix.
    begin_obs : int
        index of first obs. default 0 if only spatial analysis.

    Returns
    -------
    xa_OI : array(float)
        analysis vector.
    Pa_OI : array(float)
        analysis covariance.
    d_o_b : array(float)
        innovation vector.
    d_o_a : array(float)
        obs-analysis.
    H : array(int)
        operator to change dimensionality.
    p : int
        number of finite observations.
    y : array(float)
        finite observations.
    """
    
    # state and observation dimensions
    is_obs = np.isfinite(yo) # index of observations
    y = yo[is_obs] # observations
    n = len(xb) # number of states
    p = len(y) # number of observations

    # observation operator H 
    H = np.zeros((p,n))     
    wis_obs = np.array(np.where(is_obs))
    for i in range(0,p):
        i_tmp = wis_obs[0,i]
        H[i,i_tmp+begin_obs] = 1
    # OI analysis # 
    
    # matrix to inverse
    mat = R + H.dot(B).dot(np.transpose(H)) 

    try :
        inv = np.linalg.inv(mat)
    except Exception as e :
        print(e + "\n now using inv_using_SVD istead of np.linalg.inv")
        inv = inv_using_SVD(mat,0.95)

    #get informed of memory consumption druing matrix inversion
    #print("ressource used : ", resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
        
    #Kalman gain
    K = B.dot(np.transpose(H)).dot(inv)
    
    # analysis equations
    xa_OI = xb + K.dot(y - H.dot(xb))
    Pa_OI = np.diag(B - K.dot(H).dot(B))

    # innovation vectors
    d_o_b = y - H.dot(xb)
    d_o_a = y - H.dot(xa_OI)

    return xa_OI, Pa_OI, d_o_b, d_o_a, H, p, y

# Desroziers method to estimate R
def desroziers(yo, R, xb, B, begin_obs, nb_iter):
    """
    Parameters
    ----------
    yo : array(flaot)
        observation vector.
    R : array(flaot)
        observation covariance matrix.
    xb : array(flaot)
        backgroud vector.
    B : array(flaot)
        backgroud covariance matrix.
    begin_obs : int
        index of first obs. default 0 if only spatial analysis.
    nb_iter : int 
        number of interrations of Desroziers itterative method

    Returns
    -------
    xa_OI : array(float)
        analysis vector.
    Pa_OI : array(float)
        analysis covariance.
    d_o_b : array(float)
        innovation vector.
    d_o_a : array(float)
        obs-analysis.
    H : array(int)
        operator to change dimensionality.
    p : int
        number of finite observations.
    y : array(float)
        finite observations.
    """
    
    sigmao2s = []
    alphas = []
    R_ori = R
    
    for i in range(nb_iter):
        # apply OI,
        previous_B = B 
            
        xa_OI, Pa_OI, d_o_b, d_o_a, H, p, y = OI(yo, R, xb, previous_B, begin_obs)
        
        # estimate sigmao^2 using Desroziers et al. (2005) formula
        sigmao2 = sum(d_o_a*d_o_b)/p
        sigmao2s.append(sigmao2)
        # new R
        R = sigmao2*R_ori

        # estimate inflation factor
        alpha = (d_o_b.T.dot(d_o_b) - sigmao2*p) / np.trace(H.dot(B).dot(H.T))

        # new B
        B = alpha*previous_B
        alphas.append(alpha)
        print("inter " + str(i+1) + "/"  + str(nb_iter) + " - sigmao2 = " + str(sigmao2))
        print("inter " + str(i+1) + "/"  + str(nb_iter) + " - alpha = " + str(alpha))

    # apply OI with R estimates
    newB = B 
    xa_OI, Pa_OI, d_o_b, d_o_a, H, p, y = OI(yo, R, xb, B, begin_obs)
    return xa_OI, Pa_OI, R, alphas,sigmao2s, newB
