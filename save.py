#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 16:14:41 2021

@author: vincelhx
"""
import os 
import xarray as xr


def ncfile(shift,xa_u,xa_v,Pa_OI_u,Pa_OI_v,infos,xb_u_before,xb_v_before):
    """
    Saving the .nc file corresponding to the oi process associated with the setup.
        
    Parameters
    ----------
    shift : SHIFT object
        shift object containing grid dimensions and corresponding variables for both model and obs
    xa_u : 1D array
        zonal analysis.
    xa_v : 1D array
        meridional analysis.
    Pa_OI_u : 1D array
        zonal analysis variance.
    Pa_OI_v : 1D array
        meridional analysis variance.
    infos : setup structure
        contains all informations about the setup for oi
    Returns
    -------
    None.
    """  
    
    # dirs 
    path_folder = '/'.join(infos["output_fn"].split("/")[0:-1])
    os.makedirs(path_folder, exist_ok = True)
    
    # initialisation
    data = {}
    coords = {}
    attributes = {}
    
    # coords
    coords["lon"] =  ("lon_sar", shift.sar.lon_obs)
    coords["lat"] =  ("lat_sar", shift.sar.lat_obs)
    coords["LON_grid"] = (["lat_grid","lon_grid"],shift.LON_dec)
    coords["LAT_grid"] = (["lat_grid","lon_grid"],shift.LAT_dec)
    
    
                # saving model covariances 
    B_covs = infos["B_covs"]
    coords["Pb_init"] = (["Pb_dims","Pb_dims"],B_covs["Pb_init"])
    
    try : 
        coords["Pb_before_all"] = (["Pb_dims","Pb_dims"],B_covs["Pb_before_all"])
    except : 
        None
        
    try : 
        coords["Pb_localized"] = (["Pb_dims","Pb_dims"],B_covs["Pb_localized"])
    except : 
        None
        
    try : 
        coords["Pb_Desroziers"] = (["Pb_dims","Pb_dims"],B_covs["Pb_Desroziers"])
    except:
        None
    coords["R_var"] = (["spread_R"],infos["R_diag"])
    
    # data
    # data_sar
    data["wind_speed"] = (["lat_sar","lon_sar"],shift.sar.windspeed_obs)
    data["wind_streaks_orientation"] = (["lat_sar","lon_sar"],shift.sar.winddir_obs)
    data["wind_streaks_orientation_stddev"] = (["lat_sar","lon_sar"],shift.sar.stddev_obs)
    
    data["yo_u"] = (["lat_grid","lon_grid"],shift.yo_u.reshape(shift.LON_dec.shape))
    data["yo_v"] = (["lat_grid","lon_grid"],shift.yo_v.reshape(shift.LON_dec.shape))

    
    # data_OI
    data["xa_u"] = (["lat_grid","lon_grid"],xa_u.reshape(shift.LON_dec.shape))
    data["xa_v"] = (["lat_grid","lon_grid"],xa_v.reshape(shift.LON_dec.shape))
    data["Pa_u"] = (["lat_grid","lon_grid"],Pa_OI_u.reshape(shift.LON_dec.shape))
    data["Pa_v"] = (["lat_grid","lon_grid"],Pa_OI_v.reshape(shift.LON_dec.shape))
    
    # data_model
    data["zonal_model_before_shift"] = (["lat_grid","lon_grid"],xb_u_before)
    data["merid_model_before_shift"] = (["lat_grid","lon_grid"],xb_v_before)
    data["zonal_model_after_shift"] = (["lat_grid","lon_grid"],shift._XB_U_after.reshape(shift.LON_dec.shape))
    data["merid_model_after_shift"] = (["lat_grid","lon_grid"],shift._XB_V_after.reshape(shift.LON_dec.shape))
    #data["zonal_spread_model_after_shift"] = (["lat_grid","lon_grid"],infos["zonal_spread_model_after_shift"])
    #data["merid_spread_model_after_shift"] = (["lat_grid","lon_grid"],infos["merid_spread_model_after_shift"])
    #data["zonal_final_spread"] = (["lat_grid","lon_grid"],infos["zonal_final_spread"])
    #data["merid_final_spread"] = (["lat_grid","lon_grid"],infos["merid_final_spread"])

    # attributes
    attributes["description"] = "Optimal Interpolation to combine SAR L2 wind streaks with ensemble model"
    attributes["model_source"] = infos["model_name"]
    attributes["grid_resolution"] = infos["res"]
    attributes["localization"] = str(infos["localization"])
    attributes["uv_together"] = str(infos["uv_together"])
    attributes["desroziers"] = str(infos["desroziers"])
    attributes["c"] =  str(infos["c"])
    attributes["sar_filename"] =infos["full_path_sar"]
    attributes["path_associated_dataframe"] = infos["path_df"]
    attributes["time_model"] = infos["time_model"]
    attributes["time_sar"]= infos["time_sar"]
    attributes["translation_method"] = infos["translation_method"]


    attributes["alphas"] = infos["alphas"]
    attributes["sigma_o2s"]= infos["sigma_o2s"]
    
    # variables attributes
    ds = xr.Dataset(data_vars = data, coords = coords, attrs = attributes)
    for var in ["wind_speed","wind_streaks_orientation","wind_streaks_orientation_stddev","lon","lat"]:
        for key in shift.sar.ds_obs[var].attrs.keys():
            ds[var].attrs[key] = shift.sar.ds_obs[var].attrs[key]
    
    ds["LON_grid"].attrs["description"] = "longitudes on OI grid"
    ds["LAT_grid"].attrs["description"] = "latitudes on OI grid"
    ds["zonal_model_before_shift"].attrs["description"] = "normalized zonal wind estimated by the model reshaped on OI grid"
    ds["merid_model_before_shift"].attrs["description"] = "normalized meridional wind estimated by the model reshaped on OI grid"
    ds["zonal_model_after_shift"].attrs["description"] = "normalized zonal wind estimated by the model reshaped on OI grid, shifted to get closer from SAR"
    ds["merid_model_after_shift"].attrs["description"] = "normalized meridional wind estimated by the model reshaped on OI grid, shifted to get closer from SAR"
    ds["xa_u"].attrs["description"] = "normalized zonal wind after OI"
    ds["xa_v"].attrs["description"] = "normalized meridional wind after OI"
    ds["Pa_u"].attrs["description"] = "zonal wind variance after OI"
    ds["Pa_v"].attrs["description"] = "meridional wind variance after OI"
    
    ds.to_netcdf(infos["output_fn"],"w")
    
    