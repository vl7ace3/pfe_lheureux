#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 07:58:01 2021

@author: Vincent LHEUREUX
IFREMER    
"""
import numpy as np 
import matplotlib.pyplot as plt 
from scipy.signal import convolve2d

import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

"""
class SHIFT(object):
    def __init__(self,model,sar,apply=False):
        self.sar = sar
        self.model = model
        self.marge = self.sar.marge
        #GRILLES
        self.LON_grid = sar.LON_grid
        self.LAT_grid = sar.LAT_grid
        self.apply = apply
        self.isS1 = True if "rs2" not in self.sar.fn else False
              
    def find_best_recalage(self,res):
        mat = (np.sqrt((self.sar.y0_u_sar-self.model.xb_u)**2 + \
                   (self.sar.y0_v_sar-self.model.xb_v)**2)) 
            
        ## condition to apply weigthing    
        condition = (self.apply and self.isS1)
        if (condition):
            std_dev = self.sar.y0_stddev_obs
            std_dev = np.where(std_dev < 0, np.nan, std_dev)
            std_dev = np.where(std_dev > 40, np.nan, std_dev)
            d = np.nansum((1/std_dev)*mat)
            #print(d)
        
        else : 
            d = np.nansum(mat)
        if res>0.2:
            i = np.arange(-5,5,1)
            j = np.arange(-5,5,1)
        else:
            i = np.arange(-12,12,1)
            j = np.arange(-12,12,1)
        #print(d)
        i_min = 0 
        j_min = 0 
        d_min = d
    
        I,J = np.meshgrid(i,j)
        D = np.empty((I.shape))
        for ii in i:
            for jj in j:        
                u_current = np.roll(self.sar.y0_u_sar,j[jj],axis=0)
                v_current = np.roll(self.sar.y0_v_sar,j[jj],axis=0)
                u_current = np.roll(u_current,i[ii],axis=1)
                v_current = np.roll(v_current,i[ii],axis=1)   
    
                if (condition) : 
                    std_current = np.roll(std_dev,j[jj],axis=0)
                    std_current = np.roll(std_current,i[ii],axis=1)
                    dij = np.nansum((1/std_current)*(np.sqrt((u_current-self.model.xb_u)**2 + \
                                        (v_current-self.model.xb_v)**2)))
    
                else : 
                    dij = np.nansum(np.sqrt((u_current-self.model.xb_u)**2 + \
                                            (v_current-self.model.xb_v)**2)) 
                D[jj,ii] = dij
                #print(jj,ii,dij)
                if dij<d_min :
                    print("NOUVEAU MINIMUM en ", i[ii], j[jj], dij)
                    i_min = i[ii]
                    j_min = j[jj]
                    d_min = dij 
                    
                    #plt.figure();
                    #plt.streamplot(self.LON_grid,self.LAT_grid,u_current,v_current,color="red")
                    #plt.streamplot(self.LON_grid,self.LAT_grid,self.model.xb_u,self.model.xb_v,color="green")
                    #plt.xlim(np.min(self.LON_grid),np.max(self.LON_grid))
                    #plt.ylim(np.min(self.LAT_grid),np.max(self.LAT_grid))
         
                #print("Décalage de ",i, "lignes","et de ",j, "colonnes : D = ",d)
        print("meilleur décalage i,j = (", i_min, ",", j_min,")")
        return i_min,j_min,D 

    def get_u_v_after(self,i_min,j_min,res):
        u_best_model = np.roll(self.model.xb_u,-j_min,axis=0)
        v_best_model = np.roll(self.model.xb_v,-j_min,axis=0)
        u_best_model = np.roll(u_best_model,-i_min,axis=1)
        v_best_model = np.roll(v_best_model,-i_min,axis=1)
        self.u_best_model = u_best_model
        self.v_best_model = v_best_model
        idxLONmin = int((round(self.marge/res)))//2
        idxLATmin = int((round(self.marge/res)))//2
        idxLONmax = self.LON_grid.shape[1] - (idxLONmin)
        idxLATmax = self.LAT_grid.shape[0] - (idxLATmin)

        self.lon_decalage = self.LON_grid[0][idxLONmin:idxLONmax]
        self.lat_decalage = self.LAT_grid[:,0][idxLATmin:idxLATmax]
        self.LON_dec,self.LAT_dec = np.meshgrid(self.lon_decalage,self.lat_decalage)
        self._XB_U_after = u_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self._XB_V_after = v_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        self.ori_u = self.model.xb_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.ori_v = self.model.xb_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        ## SAR reshaping 
        yo_u = self.sar.y0_u_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_v = self.sar.y0_v_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_stddev = self.sar.y0_stddev_obs[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.yo_u = yo_u
        self.yo_v = yo_v
        
        
        self.u_members_after = np.empty(shape=(self._XB_U_after.flatten().shape[0],10))
        self.v_members_after = np.empty(shape=(self._XB_V_after.flatten().shape[0],10))
        for k in range(0,10):         
            current_u = np.roll(self.model.members_u[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_v = np.roll(self.model.members_v[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_u = np.roll(current_u,-i_min,axis=1)
            current_v = np.roll(current_v,-i_min,axis=1)
            current_u = current_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            current_v = current_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            self.u_members_after[:,k] = current_u.flatten()
            self.v_members_after[:,k] = current_v.flatten()
        #print(_XB_U_after.shape)
        return self._XB_U_after,self._XB_V_after,self.u_members_after,self.v_members_after,yo_u,yo_v, yo_stddev
    
    def display(self,txt=""):
        param = 20
        fig = plt.figure(figsize = (30,15));
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                  cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self.ori_u, self.ori_v,linewidth=1,  color = "green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.set_title('Before shift')
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                      cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self._XB_U_after, self._XB_V_after,linewidth=1, color = "green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.set_title('After shift')
        plt.savefig(txt)
"""


class SHIFT_all_members(object):
    def __init__(self,model,sar,apply=False):
        self.sar = sar
        self.model = model
        self.marge = self.sar.marge
        #GRILLES
        self.LON_grid = sar.LON_grid
        self.LAT_grid = sar.LAT_grid
        self.apply = apply
        self.isS1 = True if "rs2" not in self.sar.fn else False
        self.nb_numbers = 10 if model.name == "era5" else 50

              
    def find_best_recalage(self,res):
        """
        
        DO NOT USE 
        Find zonal and meridional "optimal shift" for every member using a distance metric

        Parameters
        ----------
        res : float
            Grid spacing.

        Returns
        -------
        i_min : int
            zonal shift (grid pixel number).
        j_min : int
            meridional shift ((grid pixel number).
        D : array(float)
            distance matrix of distances.
        """
        
        for memberi in range(0,self.nb_numbers) : 
                
            u_model = self.model.members_u[:,memberi].reshape(self.LON_grid.shape)
            v_model = self.model.members_v[:,memberi].reshape(self.LON_grid.shape)

            angle_model = (180/np.pi)*(np.arctan2(u_model,v_model))
            angle_model[angle_model<0]+=360
            angle_model[angle_model>360]-=360
            angle_sar = (180/np.pi)*(np.arctan2(self.sar.y0_v_sar,self.sar.y0_u_sar))
            angle_sar[angle_sar<0]+=360
            angle_sar[angle_sar>360]-=360
    
            diff = (angle_sar-angle_model)
            diff[diff>180] -=360
            diff[diff<-180] +=360
            mat = abs(diff)

    
            ## condition to apply weigthing    
            condition = (self.apply and self.isS1)
            if (condition):
                std_dev = self.sar.y0_stddev_obs
                std_dev = np.where(std_dev < 0, np.nan, std_dev)
                std_dev = np.where(std_dev > 40, np.nan, std_dev)
                d = np.nansum((1/std_dev)*mat)
                #print(d)
            
            else : 
                d = np.nansum(mat)
            if res>0.2:
                i = np.arange(-5,5,1)
                j = np.arange(-5,5,1)
            else:
                i = np.arange(-12,12,1)
                j = np.arange(-12,12,1)
            #print(d)
            i_min = 0 
            j_min = 0 
            d_min = d
        
            I,J = np.meshgrid(i,j)
            D = np.empty((I.shape))
            for ii in i:
                for jj in j:        
                    u_current = np.roll(self.sar.y0_u_sar,j[jj],axis=0)
                    v_current = np.roll(self.sar.y0_v_sar,j[jj],axis=0)
                    u_current = np.roll(u_current,i[ii],axis=1)
                    v_current = np.roll(v_current,i[ii],axis=1)
                    
                    angle_sar = (180/np.pi)*(np.arctan2(v_current,u_current))
                    angle_sar[angle_sar<0]+=360
                    angle_sar[angle_sar>360]-=360
                        
                    if (condition) : 
                        std_current = np.roll(std_dev,j[jj],axis=0)
                        std_current = np.roll(std_current,i[ii],axis=1)
     
                        diff = (angle_sar-angle_model)
                        diff[diff>180] -=360
                        diff[diff<-180] +=360
                        #mat = np.cos(diff)
                        mat = abs(diff)
                        dij = np.abs(np.nansum((1/std_current)*mat))
        
                    else : 
                        diff = (angle_sar-angle_model)
                        diff[diff>180] -=360
                        diff[diff<-180] +=360
                        #mat = np.cos(diff)
                        mat = abs(diff)
                        dij = np.abs(np.nansum(mat))
                    D[jj,ii] = dij
                    #print(jj,ii,dij)
                    if dij<d_min :
                        print("NOUVEAU MINIMUM en ", i[ii], j[jj], dij)
                        i_min = i[ii]
                        j_min = j[jj]
                        d_min = dij     
                        
                        #plt.figure();
                        #plt.streamplot(self.LON_grid,self.LAT_grid,u_current,v_current,color="red")
                        #plt.streamplot(self.LON_grid,self.LAT_grid,self.model.xb_u,self.model.xb_v,color="green")
                        #plt.xlim(np.min(self.LON_grid),np.max(self.LON_grid))
                        #plt.ylim(np.min(self.LAT_grid),np.max(self.LAT_grid))
             
                        #print("Décalage de ",i, "lignes","et de ",j, "colonnes : D = ",d)
        print("meilleur décalage i,j = (", i_min, ",", j_min,")")
        return i_min,j_min,D 

    def get_u_v_after(self,i_min,j_min,res):
        """
        DO NOT USE except get_u_v_after(0,0,res):

        Parameters
        ----------
        i_min : int
            zonal shift (grid pixel number).
        j_min : int
            meridional shift ((grid pixel number).
        res : float
            grid spacing.
            
        Returns
        -------
        _XB_U_after : array(float)
            Normalized zonal grid after shift.
        _XB_V_after : array(float)
            Normalized meridional grid after shift.
        u_members_after : array(float)
            Normalized zonal members after shift.
        v_members_after ; array(float)
            Normalized meridional members after shift.
        yo_u : array(float)
            sar normalized zonal wind.
        yo_v : array(float)
            sar normalized meridional wind.
        yo_stddev : array(float)
            sar uncertainty on wind streaks.

        """
        u_best_model = np.roll(self.model.xb_u,-j_min,axis=0)
        v_best_model = np.roll(self.model.xb_v,-j_min,axis=0)
        u_best_model = np.roll(u_best_model,-i_min,axis=1)
        v_best_model = np.roll(v_best_model,-i_min,axis=1)
        self.u_best_model = u_best_model
        self.v_best_model = v_best_model
        idxLONmin = int((round(self.marge/res)))//2
        idxLATmin = int((round(self.marge/res)))//2
        idxLONmax = self.LON_grid.shape[1] - (idxLONmin)
        idxLATmax = self.LAT_grid.shape[0] - (idxLATmin)

        self.lon_decalage = self.LON_grid[0][idxLONmin:idxLONmax]
        self.lat_decalage = self.LAT_grid[:,0][idxLATmin:idxLATmax]
        self.LON_dec,self.LAT_dec = np.meshgrid(self.lon_decalage,self.lat_decalage)
        self._XB_U_after = u_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self._XB_V_after = v_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        self.ori_u = self.model.xb_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.ori_v = self.model.xb_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        ## SAR reshaping 
        yo_u = self.sar.y0_u_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_v = self.sar.y0_v_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_stddev = self.sar.y0_stddev_obs[idxLATmin:idxLATmax,idxLONmin:idxLONmax]

        self.yo_u = yo_u
        self.yo_v = yo_v
        self.yo_stddev = yo_stddev
        
        
        self.u_members_after = np.empty(shape=(self._XB_U_after.flatten().shape[0],10))
        self.v_members_after = np.empty(shape=(self._XB_V_after.flatten().shape[0],10))
        for k in range(0,10):         
            current_u = np.roll(self.model.members_u[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_v = np.roll(self.model.members_v[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_u = np.roll(current_u,-i_min,axis=1)
            current_v = np.roll(current_v,-i_min,axis=1)
            current_u = current_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            current_v = current_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            self.u_members_after[:,k] = current_u.flatten()
            self.v_members_after[:,k] = current_v.flatten()
        
        #print('self._XB_U_after.shape',self._XB_U_after.shape)
        #print("self._XB_V_after.shape",self._XB_V_after.shape)
        #print('self.ori_u.shape',self.ori_u.shape)
        #print("self.ori_v.shape",self.ori_v.shape)  
        return self._XB_U_after,self._XB_V_after,self.u_members_after,self.v_members_after,yo_u,yo_v,yo_stddev
    
    def display(self,title=""):
        """
        Display the shift before and after

        Parameters
        ----------
        title : string
            pathName of the saved figure.

        Returns
        -------
        None.

        """
        param = 20
        fig = plt.figure(figsize = (30,15));
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                  cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self.ori_u, self.ori_v,linewidth=1,  color = "green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.set_title('Before shift')
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                      cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self._XB_U_after, self._XB_V_after,linewidth=1, color = "green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.set_title('After shift')
        plt.savefig(title)
    
    def shift_from_Gamma1(self, res, full_path_sar):
        """
        Shift every member on the SAR center with Gamma1 method

        Parameters
        ----------
        res : float
            grid resolution.
        full_path_sar : string
            DESCRIPTION.
            
            self._XB_U_after,self._XB_V_after,self.u_members_after,self.v_members_after,yo_u,yo_v,yo_stddev

        Returns
        -------
        _XB_U_after : array(float)
            Normalized zonal grid after shift.
        _XB_V_after : array(float)
            Normalized meridional grid after shift.
        u_members_after : array(float)
            Normalized zonal members after shift.
        v_members_after ; array(float)
            Normalized meridional members after shift.
        yo_u : array(float)
            sar normalized zonal wind.
        yo_v : array(float)
            sar normalized meridional wind.
        yo_stddev : array(float)
            sar uncertainty on wind streaks.

        """
        sar = self.sar
        model = self.model
        dx,dy, pmx,pmy = getKernel_Gamma1(11,True)

        
        idxLONmin = int((round(sar.marge/res)))//2
        idxLATmin = int((round(sar.marge/res)))//2
        idxLONmax = sar.LON_grid.shape[1] - (idxLONmin)
        idxLATmax = sar.LAT_grid.shape[0] - (idxLATmin)
    
        lon_decalage = sar.LON_grid[0][idxLONmin:idxLONmax]
        lat_decalage = sar.LAT_grid[:,0][idxLATmin:idxLATmax]
        self.LON_dec,self.LAT_dec = np.meshgrid(lon_decalage,lat_decalage)

        # Référence = centre du SAR
        if res == 0.25:
            #MICHAEL
            if full_path_sar == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":
                center_sar = (np.array([29]),np.array([22])) ## MICHAEL 20181010
            elif full_path_sar == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2020/303/S1A_EW_OWIM_CC_20201029T205631_20201029T205758_035016_0415BC/post_processing/nclight_L2M/s1a-ew-owi-cm-20201029t205631-20201029t205758-000003-0415BC_ll_gd.nc":
                center_sar = (np.array([14]),np.array([8]))  ## GONI 20201029
            else :
                print("ERREUR")
    
        elif res == 0.16 : 
            if full_path_sar == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2018/283/S1A_EW_OWIM_CC_20181010T114907_20181010T115212_024073_02A192/post_processing/nclight_L2M/s1a-ew-owi-cm-20181010t114907-20181010t115212-000003-02A192_ll_gd.nc":
                center_sar = (np.array([48]),np.array([36])) ## MICHAEL 20181010
            elif full_path_sar == "/home/datawork-cersat-public/cache/public/ftp/project/sarwing/processings/c39e79a/default/sentinel-1a/L2C/EW/S1A_EW_OWIM_1S/2020/303/S1A_EW_OWIM_CC_20201029T205631_20201029T205758_035016_0415BC/post_processing/nclight_L2M/s1a-ew-owi-cm-20201029t205631-20201029t205758-000003-0415BC_ll_gd.nc":
                center_sar = (np.array([25]),np.array([15])) ## GONI 20201029
            else :
                print("ERREUR")
        center_sar_i,center_sar_j = center_sar[0][0], center_sar[1][0]
    
        self.u_members_after = np.empty(shape=(self.LON_dec.flatten().shape[0],10))
        self.v_members_after = np.empty(shape=(self.LON_dec.flatten().shape[0],10))
    
        for i in range (0,10,1):
            u_member,v_member,pr_member = get_member(model,sar,i)
    
            conv1 = convolve2d(v_member,pmx[::-1, ::-1], mode='valid')[::1, ::1]
            conv2 = convolve2d(u_member,pmy[::-1, ::-1], mode='valid')[::1, ::1]
            gamma1_convolution = np.abs((conv1 - conv2)/np.size(pmy))
    
            center = np.where(gamma1_convolution == np.nanmax(gamma1_convolution))
            _ii,_jj = center[0][0], center[1][0]
    
    
            diff_ii = center_sar_i-_ii
            diff_jj = center_sar_j-_jj
    
            self.u_members_after[:,i] = u_member[idxLATmin-(diff_ii):idxLATmax-(diff_ii),idxLONmin-(diff_jj):idxLONmax-(diff_jj)].flatten()
            self.v_members_after[:,i] = v_member[idxLATmin-(diff_ii):idxLATmax-(diff_ii),idxLONmin-(diff_jj):idxLONmax-(diff_jj)].flatten()
    
        xb_u_after = np.mean(self.u_members_after,axis=1)
        xb_v_after = np.mean(self.v_members_after,axis=1)
    
        yo_u = sar.y0_u_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_v = sar.y0_v_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_stddev = sar.y0_stddev_obs[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        self.yo_u = yo_u
        self.yo_v = yo_v
        self.yo_stddev = yo_stddev
    
        self.ori_u = model.xb_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.ori_v = model.xb_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        self._XB_U_after = xb_u_after
        self._XB_V_after = xb_v_after    
        
        print('self._XB_U_after.shape',self._XB_U_after.shape)
        print("self._XB_V_after.shape",self._XB_V_after.shape)
        print('self.ori_u.shape',self.ori_u.shape)
        print("self.ori_v.shape",self.ori_v.shape)  
        
        return self._XB_U_after,self._XB_V_after,self.u_members_after,self.v_members_after,yo_u,yo_v,yo_stddev

        
def getKernel_Gamma1(n,display):
    """
    Apply gamma1 method. 

    Parameters
    ----------
    n : int
        size of the window.
    display : boolean
        True for display.

    Returns
    -------
    dx : array(float)
        x-axis.
    dy : array(float)
        y-axis.
    pmx : array(float)
        x-component of direction vector.
    pmy : array(float)
        y-component of direction vector.

    """
    print("using Gamma 1 for members")
    s=(n-1)/2; #s defines the shift to lft and right
    dx = np.arange(-s,s+1)
    dy = np.arange(-s,s+1)
    PMx, PMy = np.meshgrid(dx,dy);
    PMmag=np.sqrt(PMx**2+PMy**2);
    
    pmx,pmy = PMx/PMmag, PMy/PMmag
    pmx[np.isnan(pmx)] = 0
    pmy[np.isnan(pmy)] = 0
    
    if display:
        plt.figure(figsize=(5,5))
        plt.quiver(dx,dy,pmx,pmy) ; plt.grid()
    return dx,dy, pmx,pmy
#dx,dy, pmx,pmy = getKernel_Gamma1(11,True)



def get_member(model,sar,member_number):
    """
    Get member member_number

    Parameters
    ----------
    model : class
        era5 or mars class.
    sar : class
        sar class.
    member_number : int
        number number.

    Returns
    -------
    u_member : array(float)
        array of zonal components.
    v_member : array(float)
        array of meridional components.
    pr_member : array(float)
        array of pressures.

    """
    u_member = model.members_u[:,member_number].reshape(sar.LON_grid.shape)
    v_member = model.members_v[:,member_number].reshape(sar.LAT_grid.shape)
    pr_member = model.members_u[:,member_number].reshape(sar.LAT_grid.shape)
    return u_member,v_member,pr_member
