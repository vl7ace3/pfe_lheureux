#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 12 14:30:26 2021

@author: vincelhx
"""
from sar import SAR
from era import ERA
from mars import MARS
from shift import SHIFT
import numpy as np 
import matplotlib.pyplot as plt 
from scipy.interpolate import griddata


import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.patches as mpatches

class SHIFT2(object):
    def __init__(self,model,sar,apply=False):
        self.sar = sar
        self.model = model
        self.marge = self.sar.marge
        #GRILLES
        self.LON_grid = sar.LON_grid
        self.LAT_grid = sar.LAT_grid
        self.apply = apply
        self.isS1 = True if "rs2" not in self.sar.fn else False
        self.nb_numbers = 10 if model.name == "ERA5" else 50
              
    def find_best_recalage(self,res):
        
        angle_model = (180/np.pi)*(np.arctan2(self.model.xb_v,self.model.xb_u))
        angle_model[angle_model<0]+=360
        angle_model[angle_model>360]-=360
        angle_sar = (180/np.pi)*(np.arctan2(self.sar.y0_v_sar,self.sar.y0_u_sar))
        angle_sar[angle_sar<0]+=360
        angle_sar[angle_sar>360]-=360

        diff = (angle_sar-angle_model)
        diff[diff>180] -=360
        diff[diff<-180] +=360
        mat = abs(diff)


        ## condition to apply weigthing    
        condition = (self.apply and self.isS1)
        if (condition):
            std_dev = self.sar.y0_stddev_obs
            std_dev = np.where(std_dev < 0, np.nan, std_dev)
            std_dev = np.where(std_dev > 40, np.nan, std_dev)
            d = np.nansum((1/std_dev)*mat)
            #print(d)
        
        else : 
            d = np.nansum(mat)
        if res>0.2:
            i = np.arange(-5,5,1)
            j = np.arange(-5,5,1)
        else:
            i = np.arange(-12,12,1)
            j = np.arange(-12,12,1)
        #print(d)
        i_min = 0 
        j_min = 0 
        d_min = d
    
        I,J = np.meshgrid(i,j)
        D = np.empty((I.shape))
        for ii in i:
            for jj in j:        
                u_current = np.roll(self.sar.y0_u_sar,j[jj],axis=0)
                v_current = np.roll(self.sar.y0_v_sar,j[jj],axis=0)
                u_current = np.roll(u_current,i[ii],axis=1)
                v_current = np.roll(v_current,i[ii],axis=1)
                
                angle_sar = (180/np.pi)*(np.arctan2(v_current,u_current))
                angle_sar[angle_sar<0]+=360
                angle_sar[angle_sar>360]-=360
                
                if (condition) : 
                    std_current = np.roll(std_dev,j[jj],axis=0)
                    std_current = np.roll(std_current,i[ii],axis=1)
 
                    diff = (angle_sar-angle_model)
                    diff[diff>180] -=360
                    diff[diff<-180] +=360
                    #mat = np.cos(diff)
                    mat = abs(diff)
                    dij = np.abs(np.nansum((1/std_current)*mat))
    
                else : 
                    diff = (angle_sar-angle_model)
                    diff[diff>180] -=360
                    diff[diff<-180] +=360
                    #mat = np.cos(diff)
                    mat = abs(diff)
                    dij = np.abs(np.nansum(mat))
                D[jj,ii] = dij
                #print(jj,ii,dij)
                if dij<d_min :
                    print("NOUVEAU MINIMUM en ", i[ii], j[jj], dij)
                    i_min = i[ii]
                    j_min = j[jj]
                    d_min = dij 
                    
                    #plt.figure();
                    #plt.streamplot(self.LON_grid,self.LAT_grid,u_current,v_current,color="red")
                    #plt.streamplot(self.LON_grid,self.LAT_grid,self.model.xb_u,self.model.xb_v,color="green")
                    #plt.xlim(np.min(self.LON_grid),np.max(self.LON_grid))
                    #plt.ylim(np.min(self.LAT_grid),np.max(self.LAT_grid))
         
                #print("Décalage de ",i, "lignes","et de ",j, "colonnes : D = ",d)
        print("meilleur décalage i,j = (", i_min, ",", j_min,")")
        return i_min,j_min,D 

    def get_u_v_after(self,i_min,j_min,res):
        u_best_model = np.roll(self.model.xb_u,-j_min,axis=0)
        v_best_model = np.roll(self.model.xb_v,-j_min,axis=0)
        u_best_model = np.roll(u_best_model,-i_min,axis=1)
        v_best_model = np.roll(v_best_model,-i_min,axis=1)
        self.u_best_model = u_best_model
        self.v_best_model = v_best_model
        idxLONmin = int((round(self.marge/res)))//2
        idxLATmin = int((round(self.marge/res)))//2
        idxLONmax = self.LON_grid.shape[1] - (idxLONmin)
        idxLATmax = self.LAT_grid.shape[0] - (idxLATmin)

        self.lon_decalage = self.LON_grid[0][idxLONmin:idxLONmax]
        self.lat_decalage = self.LAT_grid[:,0][idxLATmin:idxLATmax]
        self.LON_dec,self.LAT_dec = np.meshgrid(self.lon_decalage,self.lat_decalage)
        self._XB_U_after = u_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self._XB_V_after = v_best_model[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        self.ori_u = self.model.xb_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.ori_v = self.model.xb_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        
        ## SAR reshaping 
        yo_u = self.sar.y0_u_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        yo_v = self.sar.y0_v_sar[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
        self.yo_u = yo_u
        self.yo_v = yo_v
        
        self.u_members_after = np.empty(shape=(self._XB_U_after.flatten().shape[0],10))
        self.v_members_after = np.empty(shape=(self._XB_V_after.flatten().shape[0],10))
        for k in range(0,10):         
            current_u = np.roll(self.model.members_u[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_v = np.roll(self.model.members_v[:,k].reshape(self.LON_grid.shape),-j_min,axis=0)
            current_u = np.roll(current_u,-i_min,axis=1)
            current_v = np.roll(current_v,-i_min,axis=1)
            current_u = current_u[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            current_v = current_v[idxLATmin:idxLATmax,idxLONmin:idxLONmax]
            self.u_members_after[:,k] = current_u.flatten()
            self.v_members_after[:,k] = current_v.flatten()
        #print(_XB_U_after.shape)
        return self._XB_U_after,self._XB_V_after,self.u_members_after,self.v_members_after,yo_u,yo_v
    
    def display(self,txt):
        param = 20
        fig = plt.figure(figsize = (30,15));
        ax = fig.add_subplot(1,2,1,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                  cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self.ori_u, self.ori_v,linewidth=1,color="green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 
        ax.set_title('Before shift')
        
        ax = fig.add_subplot(1,2,2,projection=ccrs.PlateCarree())
        ax.add_feature(cfeature.LAND, zorder=1, edgecolor='black', alpha = 0.5)
        ax.add_feature(cfeature.COASTLINE, zorder=1, edgecolor='black', alpha = 0.5, linewidth=1)
        gl = ax.gridlines(draw_labels=True, linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        im = ax.pcolormesh(self.sar.lon_obs,self.sar.lat_obs,self.sar.windspeed_obs, 
                      cmap = plt.cm.binary) ; 
        cbar = plt.colorbar(im)
        cbar.ax.set_ylabel("Wind speed [m/s]")        
        ax.streamplot(self.LON_dec, self.LAT_dec,self._XB_U_after, self._XB_V_after,linewidth=1, color = "green")
        ax.quiver(self.sar.lon_obs[::param],self.sar.lat_obs[::param], self.sar.u_obs[::param,::param],self.sar.v_obs[::param,::param], color="blue",headwidth =0,scale = 30 ); 

        ax.set_title('After shift')
        plt.savefig(txt)



import pandas as pd 
import warnings
warnings.filterwarnings("ignore")
import numpy as np;
import os 


def run(setup):
    model_name = setup["model_name"]
    res = setup["res"]

    
    # getting sar data
    print("Getting sar files...")
    sar = SAR(setup["full_path_sar"])
    time_sar = sar.time_obs;
    if (sar.load(res) == False) : 
        print("No data is sar file")
        #return False
    print("Getting sar files... done")
    
    # loading choosen model
    print("Getting " + model_name + " data")
    if (model_name == "era5") :
        try :
            model = ERA(sar)
            model.load(res)
            print(time_sar,model.time_model)
            print("Getting " + model_name + " data... done")
        except Exception as e: 
            print("- Problem : can't load " + model_name , e)
            #return False 
    elif (model_name == "mars") : 
        try :
            model = MARS(sar)
            model.load(res)
            print(time_sar,model.time_model)
            print("Getting " + model_name + " data... done")
        except Exception as e: 
            print("- Problem : can't load " + model_name , e)
            #return False 
    else :
        print("Invalid model : " + model_name)
        #return False
    
    # Shifting the model
    print("Model translation...")
    shift = SHIFT(model,sar,True)
    i_min,j_min,D  = shift.find_best_recalage(res)
    xb_u_after,xb_v_after,u_members_after,v_members_after,yo_u,yo_v= shift.get_u_v_after(i_min,j_min,res)
    shift.display("/home1/datahome/vlheureu/res_shift/"+setup["filename"].replace(".nc","_shift1.jpeg"))
    
    shift2 = SHIFT2(model,sar,True)
    i_min,j_min,D  = shift2.find_best_recalage(res)
    xb_u_after,xb_v_after,u_members_after,v_members_after,yo_u,yo_v= shift2.get_u_v_after(i_min,j_min,res)
    shift2.display("/home1/datahome/vlheureu/res_shift/"+setup["filename"].replace(".nc","_shift2.jpeg"))
    print("model translation... done")
    
if __name__ =='__main__':
    import os 

    #Parameters in input
    model_names = ["era5","mars"]
    
    import argparse
    parser = argparse.ArgumentParser(description = 'run oi')
    parser.add_argument('--inputfile',default = True,help='choose the inpuqt L2-SAR filename')
    parser.add_argument('--res',default = "0.16",help='choose the grid resolution')
    parser.add_argument('--model_name',default = "era5",choices = model_names, help= 'choose between %s'%(model_names))

    args = parser.parse_args()
    #print("args.lcz",args.lcz)

    filename = args.inputfile.split("/")[-1]
    year = filename.split('-')[4][0:4]

    setup = {
        "res" : float(args.res),
        "model_name" : args.model_name,
        "filename" : filename,
        "full_path_sar" : args.inputfile,
        "year" : year,
    }
    run(setup) 
