#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Vincent LHEUREUX
IFREMER    
"""

import numpy as np
import pandas as pd 
import os
#unused

# def compute_vorticity(u,v,spd) :
#     dU = np.gradient(u*spd)
#     dV = np.gradient(v*spd)
#     return dV[1] - dU[0]


def save_associated_df(xa_u,xa_v, yo_u,yo_v, yo_stddev, xb_u_after,xb_v_after,LON_dec,LAT_dec,setup):
    """
    Generate a dataframe with one row associated with a point of the grid.
    Compute angle from zonal and meridional component for background, obs, and analysis.
    Save in .csv format.
    
    Parameters
    ----------
    xa_u : array(float)
        OI meridional component.
    xa_v : array(float)
        OI zonal component.
    yo_u : array(float)
        zonal component of the SAR.
    yo_v : array(float)
        meridional component of the SAR.
    xb_u_after : array(float)
        zonal component of the model after shift.
    xb_v_after : array(float)
        meridional component of the model after shift.
    LON_dec : array(float)
        Final grid longitudes.
    LAT_dec : array(float)
        Final grid latitudes.

    Returns
    -------
    None.

    """
    #from sklearn.metrics import mean_squared_error
    
    mask = np.where(np.isfinite(yo_u.flatten()))[0]
    y0_u_ = yo_u.flatten()[mask]
    y0_v_ = yo_v.flatten()[mask]
    xb_u_after_ = xb_u_after.flatten()[mask]
    xb_v_after_ = xb_v_after.flatten()[mask]
    xa_u_ = xa_u[mask]
    xa_v_ = xa_v[mask]

    y0_dir = 270 - (180/np.pi)*(np.arctan2(y0_v_,y0_u_))
    y0_dir[y0_dir<0]+=360
    y0_dir[y0_dir>360]-=360
    y0_stddev_ = yo_stddev[mask]
            
    xa_dir = 270 - (180/np.pi)*(np.arctan2(xa_v_,
                                           xa_u_))
    xa_dir[xa_dir<0]+=360
    xa_dir[xa_dir>360]-=360
    
    xb_dir = 270 - (180/np.pi)*(np.arctan2(xb_v_after_,
                                           xb_u_after_))
    xb_dir[xb_dir<0]+=360
    xb_dir[xb_dir>360]-=360
    
    #plt.pcolormesh(shift.LON_dec,shift.LAT_dec,test.reshape(shift.LON_dec.shape)) ; plt.colorbar()
    
    #rmse_u = mean_squared_error(yo_u_,xa_u_,squared=False)
    #rmse_v = mean_squared_error(yo_v_,xa_v_,squared=False)
                                  
    #rmse_us.append(rmse_u)
    #rmse_vs.append(rmse_v)
                                     
    lons = LON_dec.flatten()[mask]
    lats = LAT_dec.flatten()[mask]                
    df = pd.DataFrame()
    df["lons"] = lons
    df["lats"] = lats
    df["y0_u"] = y0_u_
    df["y0_v"] = y0_v_
    df["y0_dir"] = y0_dir
    df["y0_stddev"] = y0_stddev_
                             
    df["xa_u"] =  xa_u_
    df["xa_v"] =  xa_v_
    df["xa_dir"] = xa_dir
                                                                   
    df["xb_u"] =  xb_u_after_
    df["xb_v"] =  xb_v_after_
    df["xb_dir"] = xb_dir


    df["d_a_o"] = df["xa_dir"]-df["y0_dir"]
    df["d_a_o"] = df["d_a_o"].apply(lambda x : x-360 if x > 180 else (x+360 if x<-180 else x))
    
    df["d_a_b"] = df["xa_dir"]-df["xb_dir"]
    df["d_a_b"] = df["d_a_b"].apply(lambda x : x-360 if x > 180 else (x+360 if x<-180 else x))
    
    df["d_b_o"] = df["xb_dir"]-df["y0_dir"]
    df["d_b_o"] = df["d_b_o"].apply(lambda x : x-360 if x > 180 else (x+360 if x<-180 else x))
    
        
    os.makedirs('/'.join(setup["output_fn"].replace(".nc",".csv").replace("/results/","/analysis/").split('/')[0:-1]), exist_ok = True)                                                           
    path_df = setup["output_fn"].replace(".nc",".csv").replace("/results/","/analysis/")
    df.to_csv(path_df) 
    return path_df


def getOutput(path_saving,setup, additional_txt="",intermediate_folder="/"):
    """
    Get L3-product path

    Parameters
    ----------
    path_saving : string
        saving path folder.
    setup : object
        dictionnary of the setup.
    additional_txt : string, optional
        The default is "".
    intermediate_folder : string, optional
        The default is "/".

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    lcz = "__lcz"
    if setup["localization"] == False : 
        lcz = "nolcz"
    uv_ = "sep"
    if setup["uv_together"]: 
        uv_ = "tog"
    desroz = "noDesroziers"
    if setup["desroziers"]:
        desroz = "__Desroziers"
    return path_saving+ str(setup["res"]) + "/" + str(setup["year"]) +"/"+setup["filename"].replace(".nc","/")+"/"+intermediate_folder+"/"+setup["filename"].replace('cm',"cm_oi_"+setup["model_name"]+"_"+str(setup["res"])+"_"+lcz+"_"+str(setup["c"])+"_"+uv_+"_"+desroz+additional_txt)     


